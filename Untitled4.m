% Project bounds
D = 100;
bulk = ones(4*D,1);
shear = ones(4*D,1);

young = ones(4*D,1);
poiss = ones(4*D,1);

bulk(1) = 0;
shear(1) = 0;
young(1) = 0;
poiss(1) = -1;

for d = 1:D
    %bulk(d) = 0.714 * d / D;
    %shear(d) = 0;
    
    %bulk(d+D) = 0;
    %shear(d+D) = 0.385 * d / D;
   
    bulk(d+2*D) = 0.714 * d / D;
    shear(d+2*D) = 0.385;
    
    bulk(d+3*D) = 0.714;
    shear(d+3*D) = 0.385 * d / D;
end

for l = 1:4*D
     poiss(l) = (bulk(l) - shear(l))/(bulk(l) + shear(l));
     young(l) = 4. / (1./bulk(l) + 1./shear(l));
end



%save('sweep_results_file.mat','bulk','shear')
%print('topologies/square_cell/sweep', '-dpng');

figure(2);
scatter(bulk,shear,'red','filled');
%axis([0 1 0 1])
grid on 

figure(3);
scatter(poiss,young,'blue','filled');
%axis([-1 1 0 1])
grid on


