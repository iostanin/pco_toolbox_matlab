function cl_primitives = clone_hex( side, primitives )
% This function clones equal-sided triangle cell onto isotropic rect cell
% with 6-fold symmetry

% b) Rotational Symmetries
 
 cl_primitives = primitives;
 
 
 % Center point
 x3 =  0.5 * sqrt(3.) * side;
 y3 =  1.5 * side;
 center = [y3, x3];
 
 size_i = double( 2. * round(sqrt(3)* side/2., 0) );
 size_j = double(3. * side);
 %center = [  0.5 * (1 + size_j)  0.5 * (1 + size_i)];
 
 
 si = size(primitives);
 for i = 2:si(1) % for every primitive
     
     prim = [ primitives(i,1) primitives(i,2) primitives(i,3) primitives(i,4) primitives(i,5) primitives(i,6)];
     
     %1) Rotate 5 * 60 deg around center
     for angle = 60:60:360
        prim2 = rotateTriangle( center, angle, prim);
        cl_primitives = [cl_primitives; prim2];
     end
     
     %2) Translations
     
     %x1 =  0.5 * sqrt(3.) * side * (1 - p_1);
     %y1 =  1.5 * side - 0.5 * side * p_1;
     
     % 7)
     prim2 = [prim(1) + 1.5 * side  prim(2) + 0.5 * sqrt(3.) * side ...
              prim(3) + 1.5 * side  prim(4) + 0.5 * sqrt(3.) * side ...
              prim(5) + 1.5 * side  prim(6) + 0.5 * sqrt(3.) * side];     
     cl_primitives = [cl_primitives; prim2];
     
     % 8)
     prim2 = [prim(1) - 1.5 * side  prim(2) + 0.5 * sqrt(3.) * side ...
              prim(3) - 1.5 * side  prim(4) + 0.5 * sqrt(3.) * side ...
              prim(5) - 1.5 * side  prim(6) + 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     % 9)
     angle = -60;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1) + 1.5 * side  prim2(2) + 0.5 * sqrt(3.) * side ...
              prim2(3) + 1.5 * side  prim2(4) + 0.5 * sqrt(3.) * side ...
              prim2(5) + 1.5 * side  prim2(6) + 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     
     % 10)
     angle = -120;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1) + 1.5 * side  prim2(2) - 0.5 * sqrt(3.) * side ...
              prim2(3) + 1.5 * side  prim2(4) - 0.5 * sqrt(3.) * side ...
              prim2(5) + 1.5 * side  prim2(6) - 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
          
          
     % 11)
     angle = -180;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1) - 1.5 * side  prim2(2) - 0.5 * sqrt(3.) * side ...
              prim2(3) - 1.5 * side  prim2(4) - 0.5 * sqrt(3.) * side ...
              prim2(5) - 1.5 * side  prim2(6) - 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     % 12)
     angle = -180;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1) + 1.5 * side  prim2(2) - 0.5 * sqrt(3.) * side ...
              prim2(3) + 1.5 * side  prim2(4) - 0.5 * sqrt(3.) * side ...
              prim2(5) + 1.5 * side  prim2(6) - 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     % 13)
     angle = -240;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1) - 1.5 * side  prim2(2) - 0.5 * sqrt(3.) * side ...
              prim2(3) - 1.5 * side  prim2(4) - 0.5 * sqrt(3.) * side ...
              prim2(5) - 1.5 * side  prim2(6) - 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     % 14)
     angle = -300;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1) - 1.5 * side  prim2(2) + 0.5 * sqrt(3.) * side ...
              prim2(3) - 1.5 * side  prim2(4) + 0.5 * sqrt(3.) * side ...
              prim2(5) - 1.5 * side  prim2(6) + 0.5 * sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     % 15)
     angle = -180;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1)   prim2(2) - sqrt(3.) * side ...
              prim2(3)   prim2(4) - sqrt(3.) * side ...
              prim2(5)   prim2(6) - sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
     
     % 16)
     angle = 0;
     prim2 = rotateTriangle( center, angle, prim);   
     prim2 = [prim2(1)   prim2(2) + sqrt(3.) * side ...
              prim2(3)   prim2(4) + sqrt(3.) * side ...
              prim2(5)   prim2(6) + sqrt(3.) * side];
     cl_primitives = [cl_primitives; prim2];
     
 end



end

