% Hexagonal cell, Milton stripes, continuous p_2, six parameters  

function x = plotHexacellDualMilton(side, p_1, p_2, p_3, p_4, p_5, p_6)

 size_i = double( 2. * round(sqrt(3)* side/2., 0) );
 size_j = double(3. * side);
 
 %center = [ 0.5 * (1 + size_i)  0.5 * (1 + size_j) ];
 center = [ 0.5 * sqrt(3.) * side  1.5 * side ];
 
 x = zeros(size_i, size_j);
 
 % a) Plot primitives
 primitives = zeros(1,6);
 
 % left triangle 
 x1 = 0;
 y1 = side;
 
 x2 = center(1) - 0.5 * sqrt(3.) * side * (1-p_1);
 y2 = center(2) - 0.5 * side * (1-p_1);
  
 x3 = 0;
 y3 = center(2) - 0.5 * side * (1-p_1);
 
 triangle = [y1 x1 y2 x2 y3 x3];
 primitives = [primitives; triangle];
 
 
 % right triangle 
 
 x1 = 0;
 y1 = 2 * side;
 
 x2 = center(1) - 0.5 * sqrt(3.) * side * (1-p_1);
 y2 = center(2) + 0.5 * side * (1-p_1);
  
 x3 = 0;
 y3 = center(2) + 0.5 * side * (1-p_1);
 
 triangle = [y1 x1 y2 x2 y3 x3];
 primitives = [primitives; triangle];
 
 
 % Stripes area
 
 xh = 0.5 * sqrt(3.) * side * p_1 - sqrt(3.) * p_1 * (1-p_3) * side;
 yh = 1.5 * side - 0.5 * side * (1-p_1)-1;
 
 xl = - 0.5 * sqrt(3.) * side * p_1 + sqrt(3.) * p_1 * (1-p_3) * side;
 yl = 1.5 * side + 0.5 * side * (1-p_1)+1;
 
 xk = - 0.5 * sqrt(3.) * side * p_1;
 yk = 1.5 * side - 0.5 * side * (1-p_1)-1;
 
 xe = 0.5 * sqrt(3.) * side * p_1;
 ye = 1.5 * side + 0.5 * side * (1-p_1)+1;
 
 
  % mark stripes area 
  %triangle = [yh xh yk xk ye xe];
  %primitives = [primitives; triangle];
 
  %triangle = [yk xk ye xe yl xl];
  %primitives = [primitives; triangle];
 
 % draw stripes. In new notation - p_2 = stripe width + gap width 
 
 
 
 
 
 
 % Half of a central stripe
 a = sqrt(3) * p_1 * p_3 * side;
 w = sqrt(3) * p_1 * p_2 * p_3 * side;
 wp = sqrt(3) * p_1 * p_2 * p_3 * p_4 * side;
 
 xm = 0.5 * (xh + xk); ym = yh;
 xn = 0.5 * (xe + xl); yn = ye;
 xt = xm + 0.5 * p_6 * wp; yt = ym;
 xv = xm * (1 - p_5); yv = 1.5 * side - 0.5 * side * (1-p_1) *(1-p_5);
 xr = xv + 0.5 * wp; yr = yv;
 xw = xn * (1-p_5); yw =  1.5 * side + 0.5 * side * (1-p_1) *(1-p_5);
 xs = xw + 0.5 * wp; ys = yw;
 xu = xn + 0.5 * p_6 * wp; yu = yn;
 
 
 
 %--------------------------------------
 triangle = [yt xt ym xm yr xr];
 primitives = [primitives; triangle];
 triangle = [ym xm yr xr yv xv];
 primitives = [primitives; triangle];
 
 triangle = [yr xr yv xv ys xs];
 primitives = [primitives; triangle];
 triangle = [yv xv ys xs yw xw];
 primitives = [primitives; triangle];
 
 triangle = [ys xs yw xw yu xu];
 primitives = [primitives; triangle];
 triangle = [yw xw yu xu yn xn];
 primitives = [primitives; triangle];
 
 %--------------------------------------
 
 
 i = 0;
 ext = w / 2;
 while (ext <  a/2)
    i = i + 1;
    ext = ext + w; 
    %--------------------------------------
    shr = 1.0;
    shift = 0.0;
    if ext >= a/2
       shr = 1.0 - (ext - a/2)/w;
       shift = 0.5 * (ext - a/2);
    end
    

    
    for s = -1:2:1
        
    xm = - s * shift + 0.5 * (xh + xk); ym = yh;
    xn = - s * shift + 0.5 * (xe + xl); yn = ye;
    xv = - s * shift + 0.5 * (xh + xk) * (1 - p_5); yv = 1.5 * side - 0.5 * side * (1-p_1) * (1-p_5);
    xw = - s * shift + 0.5 * (xe + xl) * (1 - p_5); yw = 1.5 * side + 0.5 * side * (1-p_1) * (1-p_5);
    
    xt = xm + 0.5 * p_6 * wp * shr; yt = ym;
    xr = xv + 0.5 * wp * shr; yr = yv;
    xs = xw + 0.5 * wp * shr; ys = yw;
    xu = xn + 0.5 * p_6 * wp * shr; yu = yn;    
        
    triangle = [yt xt + s * i * w ym xm + s * i * w yr xr + s * i * w];
    primitives = [primitives; triangle];
    triangle = [ym xm + s * i * w yr xr + s * i * w yv xv + s * i * w];
    primitives = [primitives; triangle];
 
    triangle = [yr xr + s * i * w yv xv + s * i * w ys xs + s * i * w];
    primitives = [primitives; triangle];
    triangle = [yv xv + s * i * w ys xs + s * i * w yw xw + s * i * w];
    primitives = [primitives; triangle];
 
    triangle = [ys xs + s * i * w yw xw + s * i * w yu xu + s * i * w];
    primitives = [primitives; triangle];
    triangle = [yw xw + s * i * w yu xu + s * i * w yn xn + s * i * w];
    primitives = [primitives; triangle];
    end
    %-------------------------------------- 
    
     
 end
   
 % Clone primitives to fullfill hexacell
 %cl_primitives = primitives;
 cl_primitives = clone_hex( side, primitives );

 % Plot primitives
 si = size(cl_primitives);
 for i = 2:si(1)
     
    % adjust vertical scale
    factor = double( 2. * round(sqrt(3)* side/2., 0) + 0.5) / ( sqrt(3)* side ); 
    prim = [ cl_primitives(i,1) cl_primitives(i,2) * factor cl_primitives(i,3) cl_primitives(i,4) * factor cl_primitives(i,5) cl_primitives(i,6) * factor];        
    x = x + plotTriangle(size_i, size_j, prim); 
    x = min(x, 1.);
 end
 

 
 %--- Test drawing---
 colormap(gray); 
 imagesc(1.-x); 
 caxis([0 1]); 
 axis equal; 
 axis off; 
 drawnow;
 %------------------
  
end

