function [ CR ] = LSTM( theta, vf, Emin, E0)
%% This function computes laminate stiffness matrix
theta = theta; % I would like to measure angles clockwise
C = [ (1-vf) * Emin + vf * E0 , 0.0, 0.0; ...
       0.0,  1. / ( vf / E0 + (1.- vf)/Emin ) , 0.0; ...
       0.0, 0.0, 0.5 / ( vf / E0 + (1.- vf)/Emin ) ];
 
 

T = [ cosd(theta)^2,             sind(theta)^2,          -2 * sind(theta) * cosd(theta); ...
      sind(theta)^2,             cosd(theta)^2,           2 * sind(theta) * cosd(theta); ...
      sind(theta)*cosd(theta),  - sind(theta)*cosd(theta),  cosd(theta)^2 - sind(theta)^2];
  
CR = T * C * T';  
end

