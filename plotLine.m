
% Bresenhamm line without gaps between two arbitrary points
function X = plotLine(size_x, size_y, x0,y0, x1,y1)
  X = zeros(size_x, size_y);
  
  % swap (x0,y0) and (x1,y1) if y0 > y1
  if (y0 > y1)
  y = y0; y0 = y1; y1 = y;
  x = x0; x0 = x1; x1 = x;
  
  end
  
  % Mirror vertical if dy/dx < 0 
  mirror = 0;
  if (x0 > x1)
    x0 = size_x + 1 - x0; 
    x1 = size_x + 1 - x1;
    mirror = 1;
  end
  
  % Transpose if x1 - x0 < y1 - y0 
  transp = 0;
  if (x1 - x0) <= (y1 - y0)   
    c = x0; x0 = y0; y0 = c;
    c = x1; x1 = y1; y1 = c;    
    c = size_x; size_x = size_y; size_y = c;
    transp = 1;
  end

  if (mirror==0)&&(transp==0)
      X = plotlin(size_x, size_y, x0,y0, x1,y1);
      %fprintf('1\n');  
  end
  
  if (mirror==0)&&(transp==1)
      X = plotlin(size_x, size_y, x0,y0, x1,y1);
      X = X';
      %fprintf('2\n');
  end
  
  if (mirror==1)&&(transp==1)
      X = plotlin(size_x, size_y, x0,y0, x1,y1);
      X = flipud(X');
      %fprintf('3\n');
  end
  
  if (mirror==1)&&(transp==0)
      X = plotlin(size_x, size_y, x0,y0, x1,y1);
      X = flipud(X);
      %fprintf('4\n');
  end
end
  