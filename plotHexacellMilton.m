% Hexacell with Milton style (discrete) stripes

function x = plotHexacellMilton(side, p_1, p_2, p_3, p_4, p_5, p_6)

 size_i = double( 2. * round(sqrt(3)* side/2., 0) );
 size_j = double(3. * side);
 
 x = zeros(size_i, size_j);
 
 % a) Plot primitives
 primitives = zeros(1,6);
 
 % a-1) Center triangle
  
 xd = 0.5 * sqrt(3.) * (1 - p_1) * side + 1;
 yd = side * ( 1.5 - 0.5 * p_1 ) - 1;
 
 xe = 0.5 * sqrt(3.) * (1 - p_1) * side;
 ye = side * ( 1.5 + 0.5 * p_1 ) + 1;
  
 xa = 0.5 * sqrt(3.) * side + 1.;
 ya = 1.5 * side;
 
 triangle = [ya xa yd xd ye xe];
 primitives = [primitives; triangle];
 
 % a-2) Milton-type stripes
 
 xh = 0.5 * sqrt(3.) * side * (1 - p_1) + 1;
 yh = side * (1.5 - (0.5 - p_3) * p_1);
 
 xm = 0;
 ym = side * (1.5 - 0.5 * p_1 + 0.5 * p_1 * (1 - p_3));
 
 xn = 0;
 yn = side * (1.5 + 0.5 * p_1 - 0.5 * p_1 * (1 - p_3));
 
 % test - mark the striped space
 
 %triangle = [yd xd yh xh ym xm];
 %primitives = [primitives; triangle];
 
 %triangle = [yh xh ym xm yn xn];
 %primitives = [primitives; triangle];
 
 % Central stripe 
 
 a  = p_1 * p_3 * side;
 w  = p_1 * p_2 * p_3 * side;
 wp = p_1 * p_2 * p_3 * p_4 * side;
 
 xw = 0; 
 yw = 1.5 * side + 0.5 * wp;
 
 xv = 0; 
 yv = 1.5 * side - 0.5 * wp;
 
 xu = 0.5 * sqrt(3.) * (1 - p_1) * (1 - p_5) * side ; 
 yu = 1.5 * side + 0.5 * wp - 0.5 * p_1 * (1 - p_3) * (1 - p_5) * side;
 
 xt = 0.5 * sqrt(3.) * (1 - p_1) * (1 - p_5) * side ; 
 yt = 1.5 * side - 0.5 * wp - 0.5 * p_1 * (1 - p_3) * (1 - p_5) * side;
 
 xs = 0.5 * sqrt(3.) * (1 - p_1) * side + 1; 
 ys = 1.5 * side - 0.5 * p_1 * (1 - p_3) * side + 0.5 * wp * p_6;
 
 xr = 0.5 * sqrt(3.) * (1 - p_1) * side + 1; 
 yr = 1.5 * side - 0.5 * p_1 * (1 - p_3) * side - 0.5 * wp * p_6;
 
 
 % draw a single stripe
 
 triangle = [yt xt yv xv yu xu];
 primitives = [primitives; triangle];
 
 triangle = [yv xv yu xu yw xw];
 primitives = [primitives; triangle];
 
 triangle = [yr xr yt xt ys xs];
 primitives = [primitives; triangle];
 
 triangle = [yt xt ys xs yu xu];
 primitives = [primitives; triangle];
 
 
 
 
 
 
 
 i = 0;
 ext = w / 2;
 while (ext <  a/2)
    i = i + 1;
    ext = ext + w; 
    %--------------------------------------
    shr = 1.0;
    shift = 0.0;
    if ext >= a/2
       shr = 1.0 - (ext - a/2)/w;
       shift = 0.5 * (ext - a/2);
    end
    

    
    for s = -1:2:1
        
        xw = 0; 
        yw = - s * shift + 1.5 * side + 0.5 * wp * shr;
 
        xv = 0; 
        yv = - s * shift + 1.5 * side - 0.5 * wp * shr ;
 
        xu = 0.5 * sqrt(3.) * (1 - p_1) * (1 - p_5) * side ; 
        yu = - s * shift + 1.5 * side + 0.5 * wp * shr - 0.5 * p_1 * (1 - p_3) * (1 - p_5) * side;
 
        xt = 0.5 * sqrt(3.) * (1 - p_1) * (1 - p_5) * side ; 
        yt = - s * shift + 1.5 * side - 0.5 * wp * shr - 0.5 * p_1 * (1 - p_3) * (1 - p_5) * side;
 
        xs = 0.5 * sqrt(3.) * (1 - p_1) * side + 1; 
        ys = - s * shift + 1.5 * side - 0.5 * p_1 * (1 - p_3) * side + 0.5 * wp * shr * p_6;
 
        xr = 0.5 * sqrt(3.) * (1 - p_1) * side + 1; 
        yr = - s * shift + 1.5 * side - 0.5 * p_1 * (1 - p_3) * side - 0.5 * wp * shr * p_6;
 
 
 % draw a single stripe
 
 triangle = [yt + s * i * w xt yv + s * i * w xv yu + s * i * w xu];
 primitives = [primitives; triangle];
 
 triangle = [yv + s * i * w xv yu + s * i * w xu yw + s * i * w xw];
 primitives = [primitives; triangle];
 
 triangle = [yr + s * i * w xr yt + s * i * w xt ys + s * i * w xs];
 primitives = [primitives; triangle];
 
 triangle = [yt + s * i * w xt ys + s * i * w xs yu + s * i * w xu];
 primitives = [primitives; triangle];
 
    end
    %-------------------------------------- 
    
     
 end
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 % Clone primitives to fullfill hexacell
 cl_primitives = clone_hex( side, primitives );

 % Test - plot primitives
 si = size(cl_primitives);
 for i = 2:si(1)
     
    % adjust vertical scale
    factor = double( 2. * round(sqrt(3)* side/2., 0) + 0.5) / ( sqrt(3)* side ); 
    prim = [ cl_primitives(i,1) cl_primitives(i,2) * factor cl_primitives(i,3) cl_primitives(i,4) * factor cl_primitives(i,5) cl_primitives(i,6) * factor];        
    x = x + plotTriangle(size_i, size_j, prim); x = min(x, 1.);
 end
 

 
 colormap(gray); 
 imagesc(1.-x); 
 caxis([0 1]); 
 axis equal; 
 axis off; 
 drawnow;
  
end

