% Save some nice pictures

% G-closure with volume fractions colormap
figure(2);
colormap jet;

scatter(bulk,shear,[],volfrac,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar
print('sweep_results/bulk_shear_volfrac', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],volfrac,'filled');
axis([-1 1 0 1])
grid on
colorbar
print('sweep_results/young_poiss_volfrac', '-dpng');

% G-closure with isotropy colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],iso,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar
print('sweep_results/bulk_shear_iso', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],iso,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_iso', '-dpng');

% G-closure with p_1 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_1,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_1', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_1,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_1', '-dpng');

% G-closure with p_2 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_2,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_2', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_2,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_2', '-dpng');

% G-closure with p_3 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_3,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_3', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_3,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_3', '-dpng');

% G-closure - GENERAL
figure(2);
scatter(bulk,shear,'red');
axis([0 K_base 0 G_base])
grid on 
print('sweep_results/bulk_shear', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,'blue');
axis([-1 1 0 1])
grid on
print('sweep_results/young_poiss', '-dpng');
