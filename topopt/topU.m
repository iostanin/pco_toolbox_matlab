% PERIODIC MATERIAL MICROSTRUCTURE DESIGN - OPTIMIZATION FOR 
% THE ARBITRARY ELASTIC TENSOR
% Xia, 2015, Ostanin, 2017
% ADAPTED TO BE USED WITH SWEEP SCRIPT

% Elastic constants relations assume 2D plane stress, all the relations 
% can be found in Jasiuk 1994 "Elastic moduli of 2D materials..."


function [BS] = topU(nelx, nely, volfrac, penal, rmin,ft, bulk_m, shear_m, guess_rad)

%% INPUT PARAMETERS
% nelx, nely - size of the rectangular periodic box (in unit square finite elements)
% volfrac - desirable volume fraction
% if td_volfrac = 0 - it automatically enforces
% if td_volfrac = 1 - it slowly descends to the desirable volume fraction from
% the prescribed tuning parameters max_volfrac


%% TUNING PARAMETERS OF THE ALGORITHM

max_volfrac = 0.8;                    % Initial volume fraction constraint

sym = 1;                              % 0/1 Enforce square symmetry of the distribution

                                      % !enforcing symmetry is not
                                      % necessary and can limit available
                                      % search space!
                                      
td_penal = 1;                         % 1 - penalization grows with the loop
td_ft = 0;                            % 2 - filtering radius diminishes with loop
td_volfrac = 0;                       % 3 - volume fraction diminishes with the loop
min_volfrac = 0.0;                    % Stop decreasing volfrac if its smaller than the lower bound

if td_volfrac == 1
    min_volfrac = volfrac;
    volfrac = max_volfrac;
end


max_iter = 200;                       % Maximum number of iterations   
move = 0.1;                           % Optimization step

% Initial guess

% As an initial guess we chose a circle with twice decreased initial
% density, positioned at (guess_x,guess_y) with radius guess_rad.
% For square domains when symmetry enforced, only 1/8 of the domain 
% is considered, the rest 7/8 will be tiled so the initial guess can be erased!

guess_rad = 0.4;
guess_x = 0.0;
guess_y = 0.0;


%% LOG FOR COST FUNCTION

% We store logs for cost functional and isotropy measure
cost_log = 0;
iso_log = 0;
loop_log = 0;


%% BASE/ERSATZ MATERIAL PROPERTIES
E0 = 1;
Emin = 1e-9;
nu = 0.3;


%% PREPARE FINITE ELEMENT ANALYSIS

% Element stiffness matrix
A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);

nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
edofMat = repmat(edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);

%% PREPARE FILTER
iH = ones(nelx*nely*(2*(ceil(rmin)-1)+1)^2,1);
jH = ones(size(iH));
sH = zeros(size(iH));
k = 0;
for i1 = 1:nelx
  for j1 = 1:nely
    e1 = (i1-1)*nely+j1;
    for i2 = max(i1-(ceil(rmin)-1),1):min(i1+(ceil(rmin)-1),nelx)
      for j2 = max(j1-(ceil(rmin)-1),1):min(j1+(ceil(rmin)-1),nely)
        e2 = (i2-1)*nely+j2;
        k = k+1;
        iH(k) = e1;
        jH(k) = e2;
        sH(k) = max(0,rmin-sqrt((i1-i2)^2+(j1-j2)^2));
      end
    end
  end
end
H = sparse(iH,jH,sH);
Hs = sum(H,2);

%% PERIODIC BOUNDARY CONDITIONS
e0 = eye(3);
ufixed = zeros(8,3);
U = zeros(2*(nely+1)*(nelx+1),3);
alldofs = (1:2*(nely+1)*(nelx+1));
n1 = [nodenrs(end,[1,end]),nodenrs(1,[end,1])];
d1 = reshape([(2*n1-1);2*n1],1,8);
n3 = [nodenrs(2:end-1,1)',nodenrs(end,2:end-1)];
d3 = reshape([(2*n3-1);2*n3],1,2*(nelx+nely-2));
n4 = [nodenrs(2:end-1,end)',nodenrs(1,2:end-1)];
d4 = reshape([(2*n4-1);2*n4],1,2*(nelx+nely-2));
d2 = setdiff(alldofs,[d1,d3,d4]);
for j = 1:3
  ufixed(3:4,j) =[e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[nelx;0];
  ufixed(7:8,j) = [e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[0;nely];
  ufixed(5:6,j) = ufixed(3:4,j)+ufixed(7:8,j);
end
wfixed = [repmat(ufixed(3:4,:),nely-1,1); repmat(ufixed(7:8,:),nelx-1,1)];

%% INITIALIZE ITERATION
qe = cell(3,3);
Q = zeros(3,3);
Q2 = zeros(3,3);
dQ = cell(3,3);
dQ2 = cell(3,3);

x = imread('initial guesses/35.bmp') + zeros(200,200) ;
x = ones(200,200) - x;
x = 0.5 * x + 0.5 * ones(200,200);

%x = ones(nely,nelx);
%for i = 1:nelx
%  for j = 1:nely
%    if sqrt((i-nelx/2-0.5 + guess_x * nelx/2.)^2+(j-nely/2-0.5 + guess_y * nely/2.)^2) < guess_rad * min(nelx,nely)
%      x(j,i) = 0;
%    end
%  end
%end

%x = repmat(volfrac,nely,nelx);
%for i = 1:nelx
%  for j = 1:nely
%    if sqrt((i-nelx/2-0.5 + guess_x * nelx/2.)^2+(j-nely/2-0.5 + guess_y * nely/2.)^2) < guess_rad * min(nelx,nely)
%      x(j,i) = volfrac/2;
%    end
%  end
%end



xPhys = x;
change = 1;
loop = 0;
%% START ITERATION
while ((change > 0.001) && (loop < max_iter))
  loop = loop+1;
  loop_log = [loop_log, loop];
  
  %% FE-ANALYSIS
  
      
  
  if (td_penal == 1) 
      penal = penal+0.1;
  end
  if (td_ft == 1) 
      rmin = rmin * 0.98;
  end
  
  if (td_volfrac == 1)&&(volfrac > min_volfrac) 
      volfrac = volfrac * 0.99;
  end
  
  sK = reshape(KE(:)*(Emin+xPhys(:)'.^penal*(E0-Emin)),64*nelx*nely,1);
  K = sparse(iK,jK,sK); K = (K+K')/2;
  Kr = [K(d2,d2), K(d2,d3)+K(d2,d4); K(d3,d2)+K(d4,d2), K(d3,d3)+K(d4,d3)+K(d3,d4)+K(d4,d4)];
  U(d1,:) = ufixed;
  U([d2,d3],:) = Kr\(-[K(d2,d1); K(d3,d1)+K(d4,d1)]*ufixed-[K(d2,d4); K(d3,d4)+K(d4,d4)]*wfixed);
  U(d4,:) = U(d3,:)+wfixed;
  %% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
  
  
  % Desired tensor of elasticity
  nu_m = (bulk_m - shear_m)/(bulk_m + shear_m);
  E_m = 2 * (1 - nu_m) * bulk_m;

  C_goal = zeros(3,3); 
  CC = E_m / (1. - nu_m * nu_m);
  C_goal(1,1) = CC;
  C_goal(2,2) = CC;
  C_goal(1,2) = nu_m * CC;
  C_goal(2,1) = nu_m * CC;
  C_goal(3,3) = ((1.-nu_m)/2.) * CC; 
  
  for i = 1:3
    for j = 1:3
      U1 = U(:,i); U2 = U(:,j);
      qe{i,j} = reshape(sum((U1(edofMat)*KE).*U2(edofMat),2),nely,nelx)/(nelx*nely);
      Q(i,j) = sum(sum((Emin+xPhys.^penal*(E0-Emin)).*qe{i,j}));
      Q2(i,j) = (Q(i,j)-C_goal(i,j))^2.;
      dQ{i,j} =  penal*(E0-Emin)*xPhys.^(penal-1).*qe{i,j};
      dQ2{i,j} = 2 * (Q(i,j)-C_goal(i,j)) * dQ{i,j};
    end
  end
  
  
  
  % 1) L2 dist to desirable bulk and shear
  c = 0;
  for i = 1:3
      for j = 1:3
        c = c + Q2(i,j);
      end
  end
  
  dc = dQ2{1,1} + dQ2{2,2} + dQ2{3,3} + ...
       dQ2{1,2} + dQ2{1,3} + dQ2{2,3} + ...
       dQ2{2,1} + dQ2{3,1} + dQ2{3,2};
  
   % 2) Bulk/shear mixture
   c = - bulk_m*(Q(1,1)+Q(2,2)+Q(1,2)+Q(2,1)) - shear_m*(Q(3,3));
   dc = - bulk_m*(dQ{1,1}+dQ{2,2}+dQ{1,2}+dQ{2,1}) - shear_m*(dQ{3,3});
   
   % 3) Negative poisson's ratio (Xia style)
   %c = Q(1,2)-(0.8^loop) * (Q(1,1)+Q(2,2));
   %dc = dQ{1,2}-(0.8^loop) * (dQ{1,1}+dQ{2,2});
   
  
  cost_log = [cost_log, c];
  iso_log = [iso_log, Q(1,2) + 2 * Q(3,3) - Q(1,1)];
  
  dv = ones(nely,nelx);
  %% FILTERING/MODIFICATION OF SENSITIVITIES
  
  % Stochastic shake
  dc(:) = dc(:).*(1 + (0.9^loop)*50000000.*abs(randn(size(dc(:)))));
  
  if ft == 1
    dc(:) = H*(x(:).*dc(:))./Hs./max(1e-3,x(:));
  elseif ft == 2
    dc(:) = H*(dc(:)./Hs);
    dv(:) = H*(dv(:)./Hs);
  end
  %% OPTIMALITY CRITERIA UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
  l1 = 0; l2 = 1e9;
  while (l2-l1 > 1e-9)
    lmid = 0.5*(l2+l1);
    xnew = max(0,max(x-move,min(1,min(x+move,x.*sqrt(-dc./dv/lmid)))));
    %xnew = max(0,max(x-move,min(1,min(x+move,x.*abs(sqrt(-dc./dv/lmid))))));
    %xnew = max(0,max(x-move,min(1,min(x+move,x.*(-dc./dv/lmid)))));
      % ENFORCE SYMMETRY
    if (sym == 1)
      
      % Sym in a quarter
      for i = 2:nelx/2
          for j = 1:i-1
              xnew(i,j) = xnew(j,i);
          end
      end
      
      
      for i = 1:nelx/2
          for j = 1:nely/2
              xnew(nelx + 1 - i, nely + 1 - j) = xnew(i,j);
              xnew(i, nely + 1 - j) = xnew(i,j);
              xnew(nelx + 1 - i, j) = xnew(i,j);
              
          end
      end
    end
    
    
    
    
    if ft == 1
      xPhys = xnew;
    elseif ft == 2
      xPhys(:) = (H*xnew(:))./Hs;
    end
    if mean(xPhys(:)) > volfrac, l1 = lmid; else l2 = lmid; end
  end
  change = max(abs(xnew(:)-x(:)));
  x = xnew;
  
  
  
  
%% TOPOLOGY PARAMETERS QUICKSHEET
  fprintf('HOMOGENIZED ELASTIC CONSTANTS\n');
  fprintf('\n');
  fprintf('Q11: %11.4f\n', Q(1,1));
  fprintf('Q12: %11.4f\n', Q(1,2));
  fprintf('Q13: %11.4f\n', Q(1,3));
  
  fprintf('Q21: %11.4f\n', Q(2,1));
  fprintf('Q22: %11.4f\n', Q(2,2));
  fprintf('Q23: %11.4f\n', Q(2,3));
  
  fprintf('Q31: %11.4f\n', Q(3,1));
  fprintf('Q32: %11.4f\n', Q(3,2));
  fprintf('Q33: %11.4f\n', Q(3,3));
  
  fprintf('ISOTROPY (RELATIVE)\n');
  iso = abs( (Q(1,2) + 2 * Q(3,3) - Q(1,1))/Q(1,1));
  fprintf('iso: %11.4f\n', iso);

  %% BULK AND SHEAR MODULI
  fprintf('BULK AND SHEAR (ABSOLUTE)\n');
  bulk = 0.25 * ( Q(1,1) + Q(1,2) + Q(2,1) + Q(2,2));
  shear = Q(3,3);
  fprintf('BULK: %11.4f SHEAR %11.4f  \n', bulk, shear);
  
    %% POISSON
  fprintf('POISSON (ABSOLUTE)\n');
  poiss = ( bulk - shear ) / ( bulk + shear );
  
  fprintf('POISS: %11.4f \n', poiss);
  
  %% VOLUME FRACTION
  ro =  mean(xPhys(:));
  fprintf('volfrac: %11.4f\n', ro);
 
  k_2 = E0 / (2*(1-nu));
  k_1 = Emin / (2*(1-nu));
  
  m_2 = E0 / (2*(1+nu));
  m_1 = Emin / (2*(1+nu));
  
  %fprintf('k_2: %11.4f k_1: %11.4f m_2: %11.4f m_1: %11.4f\n', k_2, k_1, m_2, m_1);
  
  fprintf('BULK AND SHEAR (W/R TO HS BOUNDS), PERSENTS\n');
  
  % HASHIN-SHTRICKMAN BOUNDS
  k_U = k_2 + (1.-ro)/((1./(k_1 - k_2))+(ro/(k_2 + m_2)));
  k_L = k_1 + ro /((1./(k_2 - k_1))+((1. - ro)/(k_1 + m_1)));
  m_U = m_2 + (1.-ro) /((1./(m_1 - m_2))+((ro*(k_2 + 2.*m_2))/(2.*m_2*(k_2 + m_2))));
  m_L = m_1 + ro /((1./(m_2 - m_1))+(((1.-ro)*(k_1 + 2.*m_1))/(2.*m_1*(k_1 + m_1))));
  
  %fprintf('k_U: %11.4f k_L: %11.4f m_U: %11.4f m_L: %11.4f\n', k_U, k_L, m_U, m_L);
  
  bulk_pers = 100 * (bulk - k_L) / (k_U - k_L);
  shear_pers = 100 * (shear - m_L) / (m_U - m_L);
  
  fprintf('BULK: %11.4f persent,             SHEAR %11.4f persent \n', bulk_pers, shear_pers);
  
 imwrite(logical(ones(nelx, nely) - xPhys),'initial guesses/20004.bmp')
  
  figure(1);
  %% PLOT DENSITIES
  colormap(gray); imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
  %% PRINT RESULTING BULK AND SHEAR IN FIGURE
  title(['BULK:   ' num2str(bulk) '   SHEAR:   ' num2str(shear)]);
  
  figure(2);
  plot(loop_log, iso_log, 'red'); % Plot isotropy parameter
  hold on;
  plot(loop_log, cost_log, 'blue'); % Plot functional
  hold off;
  
end



BS = [bulk shear];
figure(1);
print(['topologies/square_cell/top_N_' num2str(nelx) '_volfrac_' num2str(volfrac) '_penal_' num2str(penal) '_rmin_' num2str(rmin) '_ft_' num2str(ft) '_bulk_m_' num2str(bulk_m) '_shear_m_' num2str(shear_m) '.png'], '-dpng');

figure(2);
print(['functionals/square_cell/top_N_' num2str(nelx) '_volfrac_' num2str(volfrac) '_penal_' num2str(penal) '_rmin_' num2str(rmin) '_ft_' num2str(ft) '_bulk_m_' num2str(bulk_m) '_shear_m_' num2str(shear_m) '.png'], '-dpng');

end

