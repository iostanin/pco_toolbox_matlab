function X = plotlin(size_x, size_y, x0,y0, x1,y1)
  X = zeros(size_x, size_y);
  dx = x1 - x0;
  dy = y1 - y0;
  D = 2*dy - dx;
  y = y0;
  for x = x0:x1
    X(x,y) = 1;
    if D > 0
      y = y + 1;
      X(x,y) = 1;
      D = D - 2*dx;
    end
    D = D + 2*dy;
  end
end

