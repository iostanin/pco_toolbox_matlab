%% Finding limit curve using fnxtr

% 1) Load file
clear;
load('sweep_fixedvf_getLam_23_44_34_extrapolation.mat')

% 2) Reshape arrays with value of elastic constants onto matrix with 
% columns corresponding to stripe width, and rows - volume fraction

for i = 1:100
    for j = 1:10
        matr_young(i,j) = young(j + 10 * (i-1));
        matr_poiss(i,j) = poiss(j + 10 * (i-1));
    end
end

% 3) Spline interpolation

for j = 1:10
    x(j) = j;
end

for i = 1:100
    % take a slise for a fixed W
    for j = 1:10
        mask(i,j) = 1;
        y_line(j) = matr_young(i,j);
        p_line(j) = matr_poiss(i,j);
    end
    
    % Extrapolate 
    young_s = csaps( x, y_line );
    young_sn = fnxtr(young_s);
    matr_young(i,11) = fnval(young_sn,12);

    poiss_s = csaps( x, p_line );
    poiss_sn = fnxtr(poiss_s);
    matr_poiss(i,11) = fnval(poiss_sn,12);
    mask(i,11) = 2;
end


YOUNG = reshape(matr_young, 100*11, 1);
POISS = reshape(matr_poiss, 100*11, 1);
MASK  = reshape(mask, 100*11, 1);

figure(2);
 colormap jet;
 scatter(POISS,YOUNG,[],MASK,'filled');
 axis([-1 1 0 E])
 grid on
colorbar


