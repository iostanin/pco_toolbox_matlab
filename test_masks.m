mask1 = plotMaskD1(300,0.97)' ; % isotropic material
mask2 = plotMaskD2(300,0.97)' ; % laminat inclined by theta
mask3 = plotMaskD3(300,0.97)' ; % laminat inclined by theta + 60
mask4 = plotMaskD4(300,0.97)' ; % laminat inclinded by theta + 120

mask2 = mask2 &(1 - mask1);
mask3 = mask3 &(1 - mask1);
mask4 = mask4 &(1 - mask1);
mask5 = 1 & (1 - mask1) & (1 - mask2) & (1-mask3) & (1-mask4);

mask = 0.25 * (mask1 +  mask2 + mask3 + mask4 + mask5);

 figure(3);
  colormap(gray); imagesc(1 - mask'); caxis([0 1]); axis equal; axis off; drawnow;
  