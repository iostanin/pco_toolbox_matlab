%% PERIODIC MATERIAL MICROSTRUCTURE DESIGN
function topS(nelx,nely,volfrac,penal,rmin,ft)

volfrac = 0.8; % Initial volume fraction constraint
min_volfrac = 0.1;

%% MATERIAL PROPERTIES
E0 = 1;
Emin = 1e-9;
nu = 0.3;
% LOG FOR COST FUNCTION
max_iter = 200;
cost_log = ones(max_iter,1);
iso_log = ones(max_iter,1);

loop_log = ones(max_iter,1);
for i=1:max_iter
    loop_log(i) = i;
end

%% PREPARE FINITE ELEMENT ANALYSIS
A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);
nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
edofMat = repmat(edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);
%% PREPARE FILTER
iH = ones(nelx*nely*(2*(ceil(rmin)-1)+1)^2,1);
jH = ones(size(iH));
sH = zeros(size(iH));
k = 0;
for i1 = 1:nelx
  for j1 = 1:nely
    e1 = (i1-1)*nely+j1;
    for i2 = max(i1-(ceil(rmin)-1),1):min(i1+(ceil(rmin)-1),nelx)
      for j2 = max(j1-(ceil(rmin)-1),1):min(j1+(ceil(rmin)-1),nely)
        e2 = (i2-1)*nely+j2;
        k = k+1;
        iH(k) = e1;
        jH(k) = e2;
        sH(k) = max(0,rmin-sqrt((i1-i2)^2+(j1-j2)^2));
      end
    end
  end
end
H = sparse(iH,jH,sH);
Hs = sum(H,2);
%% PERIODIC BOUNDARY CONDITIONS
e0 = eye(3);
ufixed = zeros(8,3);
U = zeros(2*(nely+1)*(nelx+1),3);
alldofs = (1:2*(nely+1)*(nelx+1));
n1 = [nodenrs(end,[1,end]),nodenrs(1,[end,1])];
d1 = reshape([(2*n1-1);2*n1],1,8);
n3 = [nodenrs(2:end-1,1)',nodenrs(end,2:end-1)];
d3 = reshape([(2*n3-1);2*n3],1,2*(nelx+nely-2));
n4 = [nodenrs(2:end-1,end)',nodenrs(1,2:end-1)];
d4 = reshape([(2*n4-1);2*n4],1,2*(nelx+nely-2));
d2 = setdiff(alldofs,[d1,d3,d4]);
for j = 1:3
  ufixed(3:4,j) =[e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[nelx;0];
  ufixed(7:8,j) = [e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[0;nely];
  ufixed(5:6,j) = ufixed(3:4,j)+ufixed(7:8,j);
end
wfixed = [repmat(ufixed(3:4,:),nely-1,1); repmat(ufixed(7:8,:),nelx-1,1)];
%% INITIALIZE ITERATION
qe = cell(3,3);
Q = zeros(3,3);


% Desired tensor of elasticity
E_goal = 0.8;
nu_goal = 0.2;
C_goal = zeros(3,3); 
CC = E_goal / (1. - nu_goal * nu_goal);
C_goal(1,1) = CC;
C_goal(2,2) = CC;
C_goal(1,2) = nu_goal * CC;
C_goal(2,1) = nu_goal * CC;
C_goal(3,3) = ((1.-nu_goal)/2.) * CC; 


dQ = cell(3,3);
Q2_inv = zeros(3,3);
Q2 = zeros(3,3);
dQ2_inv = cell(3,3);
dQ2 = cell(3,3);

% Initial guess for densities
x = repmat(volfrac,nely,nelx);
for i = 1:nelx
  for j = 1:nely
    if sqrt((i-nelx/2-0.5)^2+(j-nely/2-0.5)^2) < min(nelx,nely)/3
      x(j,i) = volfrac/2;
    end
  end
end
xPhys = x;
change = 1;
loop = 0;
%% START ITERATION
while ((change > 0.01) && (loop < max_iter))
  loop = loop+1;
  
   
  if (volfrac > min_volfrac) 
      volfrac = volfrac * 0.98;
  end
  %% FE-ANALYSIS
  sK = reshape(KE(:)*(Emin+xPhys(:)'.^penal*(E0-Emin)),64*nelx*nely,1);
  K = sparse(iK,jK,sK); K = (K+K')/2;
  Kr = [K(d2,d2), K(d2,d3)+K(d2,d4); K(d3,d2)+K(d4,d2), K(d3,d3)+K(d4,d3)+K(d3,d4)+K(d4,d4)];
  U(d1,:) = ufixed;
  U([d2,d3],:) = Kr\(-[K(d2,d1); K(d3,d1)+K(d4,d1)]*ufixed-[K(d2,d4); K(d3,d4)+K(d4,d4)]*wfixed);  
  U(d4,:) = U(d3,:)+wfixed;
  %% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
  for i = 1:3
    for j = 1:3
      U1 = U(:,i); U2 = U(:,j);
      qe{i,j} = reshape(sum((U1(edofMat)*KE).*U2(edofMat),2),nely,nelx)/(nelx*nely);
      Q(i,j) = sum(sum((Emin+xPhys.^penal*(E0-Emin)).*qe{i,j}));
      Q2(i,j) = (Q(i,j)-C_goal(i,j))^2.;
      Q2_inv(i,j) = 1/((Q(i,j)-C_goal(i,j))^2.);
      
      dQ{i,j} =  penal*(E0-Emin)*xPhys.^(penal-1).*qe{i,j};
      dQ2{i,j} = 2 * (Q(i,j)-C_goal(i,j)) * dQ{i,j}; 
      dQ2_inv{i,j} = - 2.* dQ{i,j} /((Q(i,j)-C_goal(i,j))^3.);        
        
    end
  end
  c = 0;
  for i = 1:3
      for j = 1:3
      c = c + Q2(i,j);
      %c = - Q2_inv(i,j);
      end
  end
  
  dc = dQ2{1,1} + dQ2{2,2} + dQ2{3,3} + ...
       dQ2{1,2} + dQ2{1,3} + dQ2{2,3} + ...
       dQ2{2,1} + dQ2{3,1} + dQ2{3,2};
  
%dc = -(dQ2_inv{1,1} + dQ2_inv{2,2} + dQ2_inv{3,3} + ...
%       dQ2_inv{1,2} + dQ2_inv{1,3} + dQ2_inv{2,3} + ...
%       dQ2_inv{2,1} + dQ2_inv{3,1} + dQ2_inv{3,2});
  
   
   
  cost_log(loop) = - Q(3,3) * Q(3,3);
  iso_log(loop) = Q(1,2) + 2 * Q(3,3) - Q(1,1);
  dv = ones(nely,nelx);
  %% FILTERING/MODIFICATION OF SENSITIVITIES
  if ft == 1
    dc(:) = H*(x(:).*dc(:))./Hs./max(1e-3,x(:));
  elseif ft == 2
    dc(:) = H*(dc(:)./Hs);
    dv(:) = H*(dv(:)./Hs);
  end
  %% OPTIMALITY CRITERIA UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
  l1 = 0; l2 = 1e9; move = 0.01;
  while (l2-l1 > 1e-9)
    lmid = 0.5*(l2+l1);
    %xnew = max(0,max(x-move,min(1,min(x+move,x.*sqrt(-dc./dv/lmid)))));
    %xnew = max(0,max(x-move,min(1,min(x+move,x.*abs(sqrt(-dc./dv/lmid))))));
     xnew = max(0,max(x-move,min(1,min(x+move,x.*(-dc./dv/lmid)))));
    if ft == 1
      xPhys = xnew;
    elseif ft == 2
      xPhys(:) = (H*xnew(:))./Hs;
    end
    if mean(xPhys(:)) > volfrac, l1 = lmid; else l2 = lmid; end
  end
  change = max(abs(xnew(:)-x(:)));
  x = xnew;
  %% PRINT RESULTS
  fprintf(' It.:%5i Obj.:%11.4f Vol.:%7.3f ch.:%7.3f\n',loop,c, mean(xPhys(:)),change);
  %% PLOT DENSITIES
  colormap(gray); imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
end

plot(loop_log, cost_log, 'red');


