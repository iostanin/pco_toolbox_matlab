function [XY] = reflect( X, Y, m, t )
%REFLECT function - reflects a point X,Y across the non-vertical line y = m * x + t 
%   Detailed explanation goes here
if m == 0
   X_p = X;
   Y_p = 2*t-Y;
else
   X_p = X + 2*( ( (Y+X/m-t)/(m+1/m)) - X);
   Y_p = Y + 2*( m *((Y+X/m-t)/(m+1/m)) + t - Y);
end
    [XY] = [X_p Y_p];
    
end

