 % GENERAL PARAMETER SWEEP - HIGHER LEVEL SCRIPT

 % ---------Input parameters--------------
 
 % getP - homogenize test structure from file
 % getSIO - Sigmund octastructure (3 parameters maximum)
 % getHEX - Hexagonal chiral Sigmund-like
 % 2 - Milton octastructure ()
 
 E = 1;  % Base Young's modulus
 nu = 0.0; % Base Poisson's ratio
 
 definition = 300; % For square structures the resulting cell will have def X def elements
                   % For hexastructures the cell will have def X sqrt(3) * def elements 
 
 %N_param = 1; % Maximum number of parameters 

 p_min  = zeros(5); % Bounds and number of samples along each parameter 
 p_max  = zeros(5);
 p_step = ones(5);
 
 p_min(1) =  0.9;
 p_max(1) =  0.9;
 p_step(1) = 0.9;
 
 p_min(2) =  0.3334;
 p_max(2) =  0.3334;
 p_step(2) = 0.1;
 
 p_min(3) =  1;
 p_max(3) =  1;
 p_step(3) = 15;
 
 
 p_min(4) = 1.00;
 p_max(4) = 0.8;
 p_step(4) = -0.05;
 
 p_min(5) = 0.3;
 p_max(5) = 0.3;
 p_step(5) =1.;
 
 p_min(6) = 1.;
 p_max(6) = 1.;
 p_step(6) = 1.1;
 
 
 verbose = 0; % Output cell parameters
 drawnow = 0; % Draw all the pictures immadiately 
 save_struct = 1; % Save the bitmaps with computed cells 
 %=========================================
                  
 % RUN THE SWEEP FUNCTION
 runsweep(@getHEXMD, definition, E, nu, p_min, p_max, p_step, verbose, drawnow, save_struct);
 
 