
clear
load('iter_sweep_14_38_45.mat')

 figure(2);
 colormap jet;
 scatter(poiss,young,[],p_3,'filled');
 axis([-1 1 0 E])
 grid on
 colorbar
%print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');

 figure(3);
 colormap jet;
 scatter(bulk,shear,[],p_3,'filled');
 axis([0 K 0 G])
 grid on
 colorbar
%print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');