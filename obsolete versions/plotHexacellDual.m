% Try to plot rectangular cell with  

function x = plotHexacellDual(side, p_1, p_2, p_3, p_4)

 size_i = double( 2. * round(sqrt(3)* side/2., 0) );
 size_j = double(3. * side);
 
 %center = [ 0.5 * (1 + size_i)  0.5 * (1 + size_j) ];
 center = [ 0.5 * sqrt(3.) * side  1.5 * side ];
 
 x = zeros(size_i, size_j);
 
 % a) Plot primitives
 primitives = zeros(1,6);
 
 % left triangle 
 x1 = 0;
 y1 = side;
 
 x2 = center(1) - 0.5 * sqrt(3.) * side * (1-p_1);
 y2 = center(2) - 0.5 * side * (1-p_1);
  
 x3 = 0;
 y3 = center(2) - 0.5 * side * (1-p_1);
 
 triangle = [y1 x1 y2 x2 y3 x3];
 primitives = [primitives; triangle];
 
 
 % right triangle 
 
 x1 = 0;
 y1 = 2 * side;
 
 x2 = center(1) - 0.5 * sqrt(3.) * side * (1-p_1);
 y2 = center(2) + 0.5 * side * (1-p_1);
  
 x3 = 0;
 y3 = center(2) + 0.5 * side * (1-p_1);
 
 triangle = [y1 x1 y2 x2 y3 x3];
 primitives = [primitives; triangle];
 
 
 % Stripes area
 
 xh = 0.5 * sqrt(3.) * side * p_1 - sqrt(3.) * p_1 * (1-p_3) * side;
 yh = 1.5 * side - 0.5 * side * (1-p_1)-1;
 
 xl = - 0.5 * sqrt(3.) * side * p_1 + sqrt(3.) * p_1 * (1-p_3) * side;
 yl = 1.5 * side + 0.5 * side * (1-p_1)+1;
 
 xk = - 0.5 * sqrt(3.) * side * p_1;
 yk = 1.5 * side - 0.5 * side * (1-p_1)-1;
 
 xe = 0.5 * sqrt(3.) * side * p_1;
 ye = 1.5 * side + 0.5 * side * (1-p_1)+1;
 
 xm = 0.5 * (xh + xk); ym = yh;
 xn = 0.5 * (xe + xl); yn = ye;
 
  % mark stripes area 
  %triangle = [yh xh yk xk ye xe];
  %primitives = [primitives; triangle];
 
  %triangle = [yk xk ye xe yl xl];
  %primitives = [primitives; triangle];
 
 % draw stripes. In new notation - p_2 = stripe width + gap width 
 
 % Central stripe
 
 
 a = sqrt(3) * p_1 * side * p_3;
 width = p_2 * a;
 
 % Right pass + central
 
 xlb = xm + 0.5 * width * p_4 ; ylb = ym;
 xub = xm - 0.5 * width * p_4 ; yub = ym;
 xle = xn + 0.5 * width * p_4 ; yle = yn;
 xue = xn - 0.5 * width * p_4 ; yue = yn;
 
 triangle = [ylb xlb yub xub yle xle];
 primitives = [primitives; triangle];
 
 triangle = [yub xub yle xle yue xue];
 primitives = [primitives; triangle];
 
 ext = width / 2;
 
 while (ext <  a/2)  
 
        
      xlb = xlb + width;
      xle = xle + width;
      xub = xub + width;
      xue = xue + width;
      
      xlb = min(xlb, xm + a/2.);
      xle = min(xle, xn + a/2.);
      
      
      triangle = [ylb xlb yub xub yle xle];
      primitives = [primitives; triangle];
 
      triangle = [yub xub yle xle yue xue];
      primitives = [primitives; triangle];
       
    ext = ext + width;  
 end
   
 % Clone primitives to fullfill hexacell
 cl_primitives = clone_hex( side, primitives );

 % Test - plot primitives
 si = size(cl_primitives);
 for i = 2:si(1)
     
    % adjust vertical scale
    factor = double( 2. * round(sqrt(3)* side/2., 0) + 0.5) / ( sqrt(3)* side ); 
    prim = [ cl_primitives(i,1) cl_primitives(i,2) * factor cl_primitives(i,3) cl_primitives(i,4) * factor cl_primitives(i,5) cl_primitives(i,6) * factor];        
    x = x + plotTriangle(size_i, size_j, prim); x = min(x, 1.);
 end
 

 
 %colormap(gray); 
 %imagesc(1.-x); 
 %caxis([0 1]); 
 %axis equal; 
 %axis off; 
 %drawnow;
  
end

