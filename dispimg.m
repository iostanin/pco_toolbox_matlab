function [] = dispimg(X) 
  colormap(gray); imagesc(1-X); caxis([0 1]); axis equal; axis off; drawnow;
end
