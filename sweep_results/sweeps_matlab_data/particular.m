 clear
 E = 1;
 nu = 0.0;
 K = E / ( 2 * (1 - nu));
 G = E / ( 2 * (1 + nu));

 
 % sweep - 1
 %load('sweep_getLamD_15_08_29_part_1.mat')
%load('sweep_getLam_17_36_38.mat')
 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 1 * ones(size(p_1)); % number of sweep
 
 
 load('sweep_getLamD_15_38_58_part_2.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);

 
 
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, ones(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 2 * ones(size(p_1))); % number of sweep
 
 
  figure(2);
  colormap jet;
  scatter(POISS,YOUNG,[],P_1,'d','LineWidth', 2);
  axis([-1 1 0 E]);
  grid on;
  colorbar;
 hold on;
 
 load('sweep_getLam_15_21_09_left2.mat')
 
 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 3 * ones(size(p_1)); % number of sweep
 
 

 scatter(POISS,YOUNG,[],P_1,'+','LineWidth',2);

%print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');


