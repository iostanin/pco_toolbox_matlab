function [ s ] = get_S( vf, w )
% Given the volume fraction vf and stripe width w - get hexagon side s

if (w == 0.5)
    s = vf
else
    s1 = (-2 * w + sqrt( 4 * w * w + 4 * vf * (1 - 2 * w))) / (2 * (1-2*w));
    s2 = (-2 * w - sqrt( 4 * w * w + 4 * vf * (1 - 2 * w))) / (2 * (1-2*w));
    if (abs(volfrac(s1, w) - vf) < 10e-4 )
        s = s1;
    else if (abs(volfrac(s2, w) - vf) < 10e-4 )
        s = s2;
    else
        fprintf("NU NE ZNAYU");    
    end
    
end      
end

