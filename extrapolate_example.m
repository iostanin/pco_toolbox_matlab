

%rng(6); 

%x = rand(1,21); 

%s = csaps(x,x.^3); 

%sn = fnxtr(s);


%fnplt(s,[-.5 1.4],3), hold on, fnplt(sn,[-.5 1.4],.5,'r',2)
%legend('cubic smoothing spline','... properly extrapolated')
%hold off

%% Finding limit curve using fnxtr


% 1) Load file
load('sweep_fixedvf_getLam_23_44_34_extrapolation.mat')

% 2) Reshape young and poiss

for i = 1:100
    for j = 1:11
        matr_young(i) = young(i + 100 * (j-1));
        matr_poiss(i) = poiss(i + 100 * (j-1));
    end
end

figure(2);
 colormap jet;
 scatter(matr_poiss, matr_young,[],'filled');
 axis([-1 1 0 E])
 grid on
colorbar




