% Maximum bulk topologies search

M = 10; % Control of bulk parameter 
N = 10; % Control of shear parameter 
K = 4; % Control of guess circle radius 0.1...0.5

bulk = ones(M*N*K+2,1);
shear = ones(M*N*K+2,1);
guess_rad = ones(M*N*K+2,1);

x(1) = 0;
y(1) = 0;

l = 2;
for m = 1:M
    for n = 1:N
        for k = 1:K
            A = topU(100,100,0.5,3,5,1, m*0.1, n*0.1, 0.1+0.1*k)
            fprintf(' BULK_M:%11.4f SHEAR_M:%11.4f\n',A(1), A(2));
  
            x(l) = A(1);
            y(l) = A(2);
            l = l+1;
        end
    end
end


save('sweep_results_file.mat','x','y')
print('topologies/square_cell/sweep', '-dpng');
scatter(x,y,'red','filled');

