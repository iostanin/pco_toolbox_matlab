% Look at all hex/dualhex data on a single plot
 clear
 E = 1;
 nu = 0.0;
 K = E / ( 2 * (1 - nu));
 G = E / ( 2 * (1 + nu));

 
 % sweep - 1 - Georgy sweep for HEX strucutre
 load('sweep_getHEX_20_05_54.mat');
 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 P_4 = p_4;
 
 size(P_3)
 size(P_4)
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 1 * ones(size(p_1)); % number of sweep
 
 

 % sweep 3 - My sweep for HEXD structure (right side)
 load('sweep_getHEXD_14_18_57.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, ones(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 3 * ones(size(p_1))); % number of sweep
 
 
 % sweep 4 - My sweep for HEXD structure (left side)
 load('sweep_getHEXD_00_14_27.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, ones(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 4 * ones(size(p_1))); % number of sweep
 
 
 % sweep 5 - My sweep for HEXD structure (left side)
 load('sweep_getHEX_16_02_15.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 5 * ones(size(p_1))); % number of sweep
 
 
 
 figure(2);
 colormap jet;
 scatter(POISS,YOUNG,[],P_2,'filled');
 %k = convhull(POISS,YOUNG);
 %plot(POISS(k),YOUNG(k))
 axis([-1 1 0 E])
 grid on
colorbar
%print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');



 figure(3);
 colormap jet;
 scatter(BULK,SHEAR,[],P_2,'filled');
 axis([0 K 0 G])
 grid on
colorbar
print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');
