% Sigmund topology analysis

% Base material props

E_base = 1;
nu_base = 0.0;
K_base = E_base / ( 2 * (1-nu_base));
G_base = E_base / ( 2 * (1+nu_base));


M = 9; % p_1 (triangle size) 0.1...1.0 
N = 20; % p_2 (number of stripes) 1...20 
K = 10; % p_3 (stripe thickness) 0.2...0.4

%D = 100;

% Initiate datasets
bulk = ones(M*N*K,1);      % 1) Bulk modulus
shear = ones(M*N*K,1);     % 2) Shear modulus
young = ones(M*N*K,1);     % 3) Young's modulus
poiss = ones(M*N*K,1);     % 4) Poisson ratio

Q_11 = ones(M*N*K,1);         % 5-13) Components of homogenized elasticity tensor   
Q_12 = ones(M*N*K,1);            
Q_13 = ones(M*N*K,1);            
Q_21 = ones(M*N*K,1);            
Q_22 = ones(M*N*K,1);            
Q_23 = ones(M*N*K,1);            
Q_31 = ones(M*N*K,1);            
Q_32 = ones(M*N*K,1);            
Q_33 = ones(M*N*K,1);           


iso = ones(M*N*K,1);       % 14) Isotropy parameter: abs( (Q(1,2) + 2 * Q(3,3) - Q(1,1))/Q(1,1))
volfrac = ones(M*N*K,1);   % 15) Volume fraction

K_max = ones(M*N*K,1);    % 16-19) HS bounds for this VF
K_min = ones(M*N*K,1);    % 16-19) HS bounds for this VF
G_max = ones(M*N*K,1);    % 16-19) HS bounds for this VF
G_min = ones(M*N*K,1);    % 16-19) HS bounds for this VF

bulk_pers = ones(M*N*K,1); % 20) Bulk modulus relative to bounds (%)
shear_pers = ones(M*N*K,1);% 21) Shear modulus relative to bounds (%)

p_1 = ones(M*N*K,1);    % 22-24) Microstructure parameters
p_2 = ones(M*N*K,1);    
p_3 = ones(M*N*K,1);    

l = 1;
for m = 1:M
    for n = 1:N
        for k = 1:K
            A = getSIW(E_base, nu_base, 200,200, m*0.1, n, 0.1*k)
            
            % Assign parameters
            bulk(l) = A(1);
            shear(l) = A(2);
            young(l) = A(3);
            poiss(l) = A(4);
            
            
            Q_11(l) = A(5);
            Q_12(l) = A(6);
            Q_13(l) = A(7);
            Q_21(l) = A(8);
            Q_22(l) = A(9);
            Q_23(l) = A(10);
            Q_31(l) = A(11);
            Q_32(l) = A(12);
            Q_33(l) = A(13);
            
            
            iso(l) = A(14);
            volfrac(l) = A(15);
            
            K_max(l) = A(16);
            K_min(l) = A(17);
            G_max(l) = A(18);
            G_min(l) = A(19);
           
            bulk_pers(l) = A(20);
            shear_pers(l) = A(21);
            
            p_1(l) = A(22);
            p_2(l) = A(23);
            p_3(l) = A(24);
       
            l = l+1;
        end
    end
end

% Boundary points visualization
%for d = 1:D
%    
%    bulk(M*N*K+d) = K_base * d / D;
%    shear(M*N*K+d) = G_base;
%    
%    bulk(M*N*K+d+D) = K_base;
%    shear(M*N*K+d+D) = G_base * d / D;
%end

%for l = 1:2*D
%     poiss(M*N*K+l) = (bulk(M*N*K+l) - shear(M*N*K+l))/(bulk(M*N*K+l) + shear(M*N*K+l));
%     young(M*N*K+l) = 4. / (1./bulk(M*N*K+l) + 1./shear(M*N*K+l));
%end

save('sweep_results_file_SIW_nu_03.mat','bulk','shear','young','poiss','Q_11','Q_12','Q_13','Q_21','Q_22','Q_23','Q_31','Q_32','Q_33','iso','volfrac','K_max','K_min','G_max','G_min','bulk_pers','shear_pers','p_1','p_2','p_3')

% Save some nice pictures

% G-closure with volume fractions colormap
figure(2);
colormap jet;

scatter(bulk,shear,[],volfrac,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar
print('sweep_results/bulk_shear_volfrac', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],volfrac,'filled');
axis([-1 1 0 1])
grid on
colorbar
print('sweep_results/young_poiss_volfrac', '-dpng');

% G-closure with isotropy colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],iso,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar
print('sweep_results/bulk_shear_iso', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],iso,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_iso', '-dpng');

% G-closure with p_1 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_1,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_1', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_1,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_1', '-dpng');

% G-closure with p_2 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_2,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_2', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_2,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_2', '-dpng');

% G-closure with p_3 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_3,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_3', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_3,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_3', '-dpng');

% G-closure - GENERAL
figure(2);
scatter(bulk,shear,'red');
axis([0 K_base 0 G_base])
grid on 
print('sweep_results/bulk_shear', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,'blue');
axis([-1 1 0 1])
grid on
print('sweep_results/young_poiss', '-dpng');
