function [ vf ] = volfrac( s , w )
% Returns volume fraction 
vf = s * s + 2 * w * s * (1 - s);

end

