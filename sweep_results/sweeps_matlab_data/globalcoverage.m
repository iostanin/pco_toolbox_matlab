 clear
 E = 1;
 nu = 0.0;
 K = E / ( 2 * (1 - nu));
 G = E / ( 2 * (1 + nu));

 
 % sweep - 1
 load('sweep_getLamD_13_26_23_right_spot_part_1.mat')

 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 1 * ones(size(p_1)); % number of sweep
 
 
  % sweep - 1
 load('sweep_getLamD_14_24_58_right_spot part_2.mat')

 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);

 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 
 
 %============================
 figure(2);
 colormap jet;
 scatter(POISS,YOUNG,[],SWEEP,'filled');
 axis([-1 1 0 E])
 grid on
colorbar
%print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');

 figure(3);
 colormap jet;
 scatter(BULK,SHEAR,[],SWEEP,'filled');
 axis([0 K 0 G])
 grid on
colorbar
print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');
