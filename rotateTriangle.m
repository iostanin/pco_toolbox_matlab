function rt = rotateTriangle(center, angle, triangle)
% This function rotates the triangle counterclockwise

v1 = [triangle(1) - center(1)  triangle(2) - center(2)];
v2 = [triangle(3) - center(1)  triangle(4) - center(2)];
v3 = [triangle(5) - center(1)  triangle(6) - center(2)];
R = [cosd(angle) -sind(angle); sind(angle) cosd(angle)];

vr1 = R * v1'; 
vr2 = R * v2'; 
vr3 = R * v3';

rt = [ (vr1(1) + center(1)) (vr1(2) + center(2)) ...
       (vr2(1) + center(1)) (vr2(2) + center(2)) ...
       (vr3(1) + center(1)) (vr3(2) + center(2)) ];
end