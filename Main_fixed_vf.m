 % PARAMETER SWEEP WITH FIXED VOLUME FRACTION AND CONTROLLED WIDTH OF
 % LAMINATION STRIPES

 % ---------Input parameters--------------
 
 % getP - homogenize test structure from file
 % getSIO - Sigmund octastructure (3 parameters maximum)
 % getHEX - Hexagonal chiral Sigmund-like
 % 2 - Milton octastructure ()
 
 E = 1;  % Base Young's modulus
 nu = 0.0; % Base Poisson's ratio
 volfrac = 0.9999; % Desirable volume fraction of base material in the structure
 definition = 300; % For square structures the resulting cell will have def X def elements
                   % For hexastructures the cell will have def X sqrt(3) * def elements 
 
 %N_param = 1; % Maximum number of parameters 

 w_min  = 0.995; % Bounds and number of samples along each parameter 
 w_max  = 1.000;
 w_step = 0.001; 
 
 verbose = 0; % Output cell parameters
 drawnow = 0; % Draw all the pictures immadiately 
 save_struct = 0; % Save the bitmaps with computed cells 
 %=========================================
                  
 % RUN THE SWEEP FUNCTION
 runsweepfixedvf(@getLam, definition, E, nu, w_min, w_max, w_step, volfrac, verbose, drawnow, save_struct);
 
 