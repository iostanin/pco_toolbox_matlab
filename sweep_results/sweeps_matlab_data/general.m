% Look at all hex/dualhex data on a single plot
 clear
 E = 1;
 nu = 0.0;
 K = E / ( 2 * (1 - nu));
 G = E / ( 2 * (1 + nu));

 
 % sweep - 1 - Georgy sweep for HEX strucutre
 load('sweep_getHEX_20_05_54.mat');
 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 P_4 = p_4;
 
 size(P_3)
 size(P_4)
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 1 * ones(size(p_1)); % number of sweep
 
 
 % sweep 2 - My sweep for HEX structure
 %load('sweep_getHEX_00_54_01.mat');
 
 %P_1 = horzcat(P_1, p_1);
 %P_2 = horzcat(P_2, p_2);
 %P_3 = horzcat(P_3, p_3);
 %P_4 = horzcat(P_4, p_4);
 
 %size(P_3)
 %size(P_4)
 
 %VOLFRAC = horzcat(VOLFRAC, volfrac);
 %ISO = horzcat(ISO, iso);
 %YOUNG = horzcat(YOUNG, young);
 %POISS = horzcat(POISS, poiss);
 %SHEAR = horzcat(SHEAR, shear);
 %BULK  = horzcat(BULK, bulk);
 
 %STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 %SWEEP = horzcat(SWEEP, 2 * ones(size(p_1))); % number of sweep
 
 
 % sweep 3 - My sweep for HEXD structure (right side)
 load('sweep_getHEXD_14_18_57.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, ones(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 3 * ones(size(p_1))); % number of sweep
 
 
 % sweep 4 - My sweep for HEXD structure (left side)
 load('sweep_getHEXD_00_14_27.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, ones(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 4 * ones(size(p_1))); % number of sweep
 
 
 % sweep 5 - My sweep for HEXD structure (left side)
 load('sweep_getHEX_16_02_15.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 5 * ones(size(p_1))); % number of sweep
 
 % Sweep 6 
 load('iter_sweep_11_21_13.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 6 * ones(size(p_1))); % number of sweep
 
 % Sweep 7 
 load('iter_sweep_14_38_45.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 7 * ones(size(p_1))); % number of sweep
 
 
 
 % Sweep 7 
 load('sweep_1024_res.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 8 * ones(size(p_1))); % number of sweep
 
 
 % Post-process P_2
 
 for i = 1:size(P_2)
     if (P_2(i)<1)
         P_2 = 1 / P_2;
     end
 end
 
 
 % Sweep 8 - Laminate LEFT 
 load('sweep_getLam_17_35_14.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 % Sweep 9 - Laminate RIGHT 
 load('sweep_getLamD_14_19_27_right_1.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, ones(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 %================================
 % Sweep 9 - Laminate RIGHT 1 0f 5 
 load('sweep_getLam_12_28_19_p2_part1.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 %================================
 % Sweep 9 - Laminate RIGHT 2 0f 5 
 load('sweep_getLam_12_47_40_p2_part2.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 %================================
 % Sweep 9 - Laminate RIGHT 3 0f 5 
  load('sweep_getLam_13_45_33_p2_part3.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 %================================
 % Sweep 9 - Laminate RIGHT 4 0f 5 
  load('sweep_getLam_14_10_02_p2_part4.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 %================================
 % Sweep 9 - Laminate RIGHT 5 0f 5 
  load('sweep_getLam_14_47_33_p2_part5.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 
 
 
 
 
 load('sweep_getLam_17_48_36.mat');
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 9 * ones(size(p_1))); % number of sweep
 
 
 
 load('sweep_getLamD_11_34_38.mat')
  
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 10 * ones(size(p_1))); % number of sweep
 
 
 %======================================================================
 
 load('sweep_getLam_15_41_11.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 11 * ones(size(p_1))); % number of sweep
 
 load('sweep_getLamD_13_26_23_right_spot_part_1.mat')
 
 P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 12 * ones(size(p_1))); % number of sweep
 
 
 
 load('sweep_getLamD_14_24_58_right_spot part_2.mat')
 
 
  P_1 = horzcat(P_1, p_1);
 P_2 = horzcat(P_2, p_2);
 P_3 = horzcat(P_3, p_3);
 P_4 = horzcat(P_4, p_4);
 
 size(P_3)
 size(P_4)
 
 VOLFRAC = horzcat(VOLFRAC, volfrac);
 ISO = horzcat(ISO, iso);
 YOUNG = horzcat(YOUNG, young);
 POISS = horzcat(POISS, poiss);
 SHEAR = horzcat(SHEAR, shear);
 BULK  = horzcat(BULK, bulk);
 
 STRUCT  = horzcat(STRUCT, zeros(size(p_1))); % zero - HEX, one - HEXD
 SWEEP = horzcat(SWEEP, 13 * ones(size(p_1))); % number of sweep
 
 
 
 
 figure(2);
 colormap jet;
 scatter(POISS,YOUNG,[],P_3,'filled');
 %k = convhull(POISS,YOUNG);
 %plot(POISS(k),YOUNG(k))
 axis([-1 1 0 E])
 grid on
colorbar
%print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');



 figure(3);
 colormap jet;
 scatter(BULK,SHEAR,[],P_3,'filled');
 axis([0 K 0 G])
 grid on
colorbar
print([ 'sweep_results/plots/general/young_poiss_p_1'], '-dpng');
