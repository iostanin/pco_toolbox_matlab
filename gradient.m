% Gradient descent

E_goal = 0.5;
nu_goal = 0.0;

% Initial guess parameters
pp_1 = 0.5;
pp_2 = 0.5;
pp_3 = 1.0;
pp_4 = 0.3;
pp_5 = 0.5;
pp_6 = 0.3;



step = 0.01;
delta = 1;
cycle = 0;
while (delta>0.0001)
    cycle = cycle+1;
    cycle
    
    % Compute gradient
    F0 = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2, pp_3, pp_4, pp_5, pp_6);
    dp = 0.004;
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1+dp, pp_2, pp_3, pp_4, pp_5, pp_6);
    dFdp1 = (1/dp) * ( f(1) - F0(1));
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2+dp, pp_3, pp_4, pp_5, pp_6);
    
    dFdp2 = (1/dp) * ( f(1) - F0(1));
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2, pp_3+dp, pp_4, pp_5, pp_6);
    dFdp3 = (1/dp) * ( f(1) - F0(1));
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2, pp_3, pp_4+dp, pp_5, pp_6);
    dFdp4 = (1/dp) * ( f(1) - F0(1));
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2, pp_3, pp_4, pp_5+dp, pp_6);
    
    dFdp5 = (1/dp) * ( f(1) - F0(1));
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2, pp_3+dp, pp_4, pp_5, pp_6+dp);
    dFdp6 = (1/dp) * ( f(1) - F0(1));
    
    fprintf('Grad: (%11.4f, %11.4f, %11.4f, %11.4f, %11.4f, %11.4f) \n\n\n', dFdp1, dFdp2, dFdp3, dFdp4, dFdp5, dFdp6);
    % Update parameters
    pp_1 = pp_1 - dFdp1 * step;
    pp_2 = pp_2 - dFdp2 * step;
    pp_3 = pp_3 - dFdp3 * step;
    pp_4 = pp_4 - dFdp4 * step;
    pp_5 = pp_5 - dFdp5 * step;
    pp_6 = pp_6 - dFdp6 * step;
    
    f = optimize_HEXM(E_goal, nu_goal, pp_1, pp_2, pp_3, pp_4, pp_5, pp_6);
    delta = abs(F0(1) - f(1));
    
    E_current = f(2);
    nu_current = f(3);
    fprintf('p_1: %11.4f p_2: %11.4f p_3: %11.4f p_4: %11.4f p_5: %11.4f p_6: %11.4f \n\n\n', pp_1, pp_2, pp_3, pp_4, pp_5, pp_6);
    fprintf('E_current: %11.4f nu_current: %11.4f Dist: %11.4f\n', E_current, nu_current, delta);
    
end    
