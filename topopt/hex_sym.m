function [ xsym ] = hex_sym( x, nel)
%HEX-SYM Enforces hexagonal symmetry of the densities in a rectangular cell

  % 0

  xnew = x;

  % A-B
  for i_x = 1:round(sqrt(3)/2*nel)
      for i_y = 1:round(i_x/sqrt(3))
          A = reflect(i_x, i_y, 1/sqrt(3), 0);
          i_xp = round(A(1));
          i_yp = round(A(2));
          
          if i_xp < 2 i_xp = 2; end
          if i_yp < 2 i_yp = 2; end
              
          %xnew(i_xp, i_yp) = xnew(i_x, i_y);
          %xnew(i_x, i_y) = 1;
          xnew(i_xp, i_yp) = xnew(i_x, i_y);
          % Antialias
          xnew(i_xp+1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp, i_yp-1) = xnew(i_x, i_y);
          
          xnew(i_xp+1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp-1) = xnew(i_x, i_y);
          xnew(i_xp+1, i_yp-1) = xnew(i_x, i_y);
          
      end
  end
  
  
  % B-C-1
  for i_x = 1:round(sqrt(3)/4*nel)
      for i_y = round(i_x/sqrt(3)):round(sqrt(3)*i_x)
          A = reflect(i_x, i_y, sqrt(3), 0);
          i_xp = round(A(1));
          i_yp = round(A(2));
          
          if i_xp < 2 i_xp = 2; end
          if i_yp < 2 i_yp = 2; end
              
          %xnew(i_xp, i_yp) = xnew(i_x, i_y);
          %xnew(i_x, i_y) = 1;
          
          xnew(i_xp, i_yp) = xnew(i_x, i_y);
          % Antialias
          xnew(i_xp+1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp, i_yp-1) = xnew(i_x, i_y);
          
          xnew(i_xp+1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp-1) = xnew(i_x, i_y);
          xnew(i_xp+1, i_yp-1) = xnew(i_x, i_y);
          
      end
  end
  
    % B-C-2
  for i_x = round(sqrt(3)/4*nel)+1:round(sqrt(3)/2*nel)
      for i_y = round(i_x/sqrt(3)): round(nel - i_x/sqrt(3))
          A = reflect(i_x, i_y, sqrt(3), 0);
          i_xp = round(A(1));
          i_yp = round(A(2));
          
          if i_xp < 2 i_xp = 2; end
          if i_yp < 2 i_yp = 2; end
              
          %xnew(i_xp, i_yp) = xnew(i_x, i_y);
          %xnew(i_x, i_y) = 1;
          
          xnew(i_xp, i_yp) = xnew(i_x, i_y);
          % Antialias
          xnew(i_xp+1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp, i_yp-1) = xnew(i_x, i_y);
          
          xnew(i_xp+1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp-1) = xnew(i_x, i_y);
          xnew(i_xp+1, i_yp-1) = xnew(i_x, i_y);
          
      end
  end

  % A-B-C-1
  for i_x = 1:round(sqrt(3)/2*nel)
      for i_y = 1:round(nel - i_x/sqrt(3))
          A = reflect(i_x, i_y, -1/sqrt(3), nel);
          i_xp = round(A(1));
          i_yp = round(A(2));
          
          if i_xp < 2 i_xp = 2; end
          if i_yp < 2 i_yp = 2; end
              
          %xnew(i_xp, i_yp) = xnew(i_x, i_y);
          %xnew(i_x, i_y) = 1;
          
          xnew(i_xp, i_yp) = xnew(i_x, i_y);
          % Antialias
          xnew(i_xp+1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp) = xnew(i_x, i_y);
          xnew(i_xp, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp, i_yp-1) = xnew(i_x, i_y);
          
          xnew(i_xp+1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp+1) = xnew(i_x, i_y);
          xnew(i_xp-1, i_yp-1) = xnew(i_x, i_y);
          xnew(i_xp+1, i_yp-1) = xnew(i_x, i_y);
          
      end
  end  
  
    % A-B-C-2
  for i_x = 1:round(sqrt(3)/2*nel)
      for i_y = 1:round(i_x/sqrt(3))
          
          i_xp = round(sqrt(3)/2*nel - i_x);
          i_yp = round(3/2.*nel - i_y);
          
          if i_xp < 1 i_xp = 1; end
          if i_yp < 1 i_yp = 1; end
          
          xnew( i_xp, i_yp) = xnew(i_x, i_y);
          
      end
  end
  
  % Flip 1

  for i_x = 1:round(sqrt(3)/2.*nel)
      for i_y = 1:round(3/2.*nel)
          
          i_xp = round(sqrt(3)*nel - i_x);
          i_yp = i_y;
          
          if i_xp < 1 i_xp = 1; end
          if i_yp < 1 i_yp = 1; end
          
          xnew( i_xp, i_yp) = xnew(i_x, i_y);
          
      end
  end
  
    % Flip 2

  for i_x = 1:round(sqrt(3).*nel)
      for i_y = 1:round(3/2.*nel)
          
          i_xp = i_x;
          i_yp = 3*nel - i_y;
          
          if i_xp < 1 i_xp = 1; end
          if i_yp < 1 i_yp = 1; end
          
          xnew( i_xp, i_yp) = xnew(i_x, i_y);
          
      end
  end
 [ xsym ] = xnew;
end

