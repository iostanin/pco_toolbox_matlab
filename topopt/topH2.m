%% PERIODIC MATERIAL MICROSTRUCTURE DESIGN
%% Hexagonal (isotropic) periodic cell
%% Example - topH2(100, 0.5,5,5,1)

function topH2(nel, volfrac,penal,rmin,ft)

%% Determine nelx, nely

nelx = double(3. * nel);
nely = double( 2. * round(sqrt(3)* nel/2., 0) );
fprintf(' nelx: %5i nely: %5i\n',nelx,nely);
 
%% LOG FOR COST FUNCTION

% We store logs for cost functional and isotropy measure
cost_log = 0;
iso_log = 0;
loop_log = 0;

%% MATERIAL PROPERTIES
E0 = 1;
Emin = 1e-9;
nu = 0.3;
%% PREPARE FINITE ELEMENT ANALYSIS
A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);
nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
edofMat = repmat(edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);
%% PREPARE FILTER
iH = ones(nelx*nely*(2*(ceil(rmin)-1)+1)^2,1);
jH = ones(size(iH));
sH = zeros(size(iH));
k = 0;
for i1 = 1:nelx
  for j1 = 1:nely
    e1 = (i1-1)*nely+j1;
    for i2 = max(i1-(ceil(rmin)-1),1):min(i1+(ceil(rmin)-1),nelx)
      for j2 = max(j1-(ceil(rmin)-1),1):min(j1+(ceil(rmin)-1),nely)
        e2 = (i2-1)*nely+j2;
        k = k+1;
        iH(k) = e1;
        jH(k) = e2;
        sH(k) = max(0,rmin-sqrt((i1-i2)^2+(j1-j2)^2));
      end
    end
  end
end
H = sparse(iH,jH,sH);
Hs = sum(H,2);
%% PERIODIC BOUNDARY CONDITIONS
e0 = eye(3);
ufixed = zeros(8,3);
U = zeros(2*(nely+1)*(nelx+1),3);
alldofs = (1:2*(nely+1)*(nelx+1));
n1 = [nodenrs(end,[1,end]),nodenrs(1,[end,1])];
d1 = reshape([(2*n1-1);2*n1],1,8);
n3 = [nodenrs(2:end-1,1)',nodenrs(end,2:end-1)];
d3 = reshape([(2*n3-1);2*n3],1,2*(nelx+nely-2));
n4 = [nodenrs(2:end-1,end)',nodenrs(1,2:end-1)];
d4 = reshape([(2*n4-1);2*n4],1,2*(nelx+nely-2));
d2 = setdiff(alldofs,[d1,d3,d4]);
for j = 1:3
  ufixed(3:4,j) =[e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[nelx;0];
  ufixed(7:8,j) = [e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[0;nely];
  ufixed(5:6,j) = ufixed(3:4,j)+ufixed(7:8,j);
end
wfixed = [repmat(ufixed(3:4,:),nely-1,1); repmat(ufixed(7:8,:),nelx-1,1)];
%% INITIALIZE ITERATION
qe = cell(3,3);
Q = zeros(3,3);
dQ = cell(3,3);


x = ones(nelx, nely) - double(imread('../2D library/34.bmp'))' ;
x = x';
v = size(x);
fprintf(' x: %5i y: %5i\n', v(1), v(2));


x = repmat(volfrac,nely,nelx);
for i = 1:nelx
  for j = 1:nely
    if sqrt((i-nelx-0.5)^2+(j-nely/2-0.5)^2) < min(nelx,nely)/3
      x(j,i) = volfrac/2;
    end
  end
end

xPhys = x;
change = 1;
loop = 0;
%% START ITERATION
while (change > 0.01)
%while (loop < 2)
    
  loop = loop+1;
  loop_log = [loop_log, loop];
  %% FE-ANALYSIS
  sK = reshape(KE(:)*(Emin+xPhys(:)'.^penal*(E0-Emin)),64.*nelx*nely,1);
  K = sparse(iK,jK,sK); K = (K+K')/2;
  Kr = [K(d2,d2), K(d2,d3)+K(d2,d4); K(d3,d2)+K(d4,d2), K(d3,d3)+K(d4,d3)+K(d3,d4)+K(d4,d4)];
  U(d1,:) = ufixed;
  U([d2,d3],:) = Kr\(-[K(d2,d1); K(d3,d1)+K(d4,d1)]*ufixed-[K(d2,d4); K(d3,d4)+K(d4,d4)]*wfixed);
  U(d4,:) = U(d3,:)+wfixed;
  %% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
  for i = 1:3
    for j = 1:3
      U1 = U(:,i); U2 = U(:,j);
      qe{i,j} = reshape(sum((U1(edofMat)*KE).*U2(edofMat),2),nely,nelx)/(nelx*nely);
      Q(i,j) = sum(sum((Emin+xPhys.^penal*(E0-Emin)).*qe{i,j}));
      dQ{i,j} =  penal*(E0-Emin)*xPhys.^(penal-1).*qe{i,j};
    end
  end
  c = -(Q(1,1)+Q(2,2)+Q(1,2)+Q(2,1));
  dc = -(dQ{1,1}+dQ{2,2}+dQ{1,2}+dQ{2,1});
  dv = ones(nely,nelx);
  
  cost_log = [cost_log, c];
  iso_log = [iso_log, Q(1,2) + 2 * Q(3,3) - Q(1,1)];
  
  %% FILTERING/MODIFICATION OF SENSITIVITIES
  if ft == 1
    dc(:) = H*(x(:).*dc(:))./Hs./max(1e-3,x(:));
  elseif ft == 2
    dc(:) = H*(dc(:)./Hs);
    dv(:) = H*(dv(:)./Hs);
  end
  %% OPTIMALITY CRITERIA UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
  l1 = 0; l2 = 1e9; move = 0.2;
  while (l2-l1 > 1e-9)
    lmid = 0.5*(l2+l1);
    xnew = max(0,max(x-move,min(1,min(x+move,x.*sqrt(-dc./dv/lmid)))));
    
    xnew = hex_sym(xnew, nel);
    
    
    if ft == 1
      xPhys = xnew;
    elseif ft == 2
      xPhys(:) = (H*xnew(:))./Hs;
    end
    if mean(xPhys(:)) > volfrac, l1 = lmid; else l2 = lmid; end
  end
  change = max(abs(xnew(:)-x(:)));
 
  
  
  
  x = xnew;
  
  
  
  
%% TOPOLOGY PARAMETERS QUICKSHEET
  fprintf('HOMOGENIZED ELASTIC CONSTANTS\n');
  fprintf('\n');
  fprintf('Q11: %11.4f\n', Q(1,1));
  fprintf('Q12: %11.4f\n', Q(1,2));
  fprintf('Q13: %11.4f\n', Q(1,3));
  
  fprintf('Q21: %11.4f\n', Q(2,1));
  fprintf('Q22: %11.4f\n', Q(2,2));
  fprintf('Q23: %11.4f\n', Q(2,3));
  
  fprintf('Q31: %11.4f\n', Q(3,1));
  fprintf('Q32: %11.4f\n', Q(3,2));
  fprintf('Q33: %11.4f\n', Q(3,3));
  
  fprintf('ISOTROPY (RELATIVE)\n');
  iso = abs( (Q(1,2) + 2 * Q(3,3) - Q(1,1))/Q(1,1));
  fprintf('iso: %11.4f\n', iso);

  %% BULK AND SHEAR MODULI
  fprintf('BULK AND SHEAR (ABSOLUTE)\n');
  bulk = 0.25 * ( Q(1,1) + Q(1,2) + Q(2,1) + Q(2,2));
  shear = Q(3,3);
  fprintf('BULK: %11.4f SHEAR %11.4f  \n', bulk, shear);
  
    %% POISSON
  fprintf('POISSON (ABSOLUTE)\n');
  poiss = ( bulk - shear ) / ( bulk + shear );
  
  fprintf('POISS: %11.4f \n', poiss);
  
  %% VOLUME FRACTION
  ro =  mean(xPhys(:));
  fprintf('volfrac: %11.4f\n', ro);
 
  k_2 = E0 / (2*(1-nu));
  k_1 = Emin / (2*(1-nu));
  
  m_2 = E0 / (2*(1+nu));
  m_1 = Emin / (2*(1+nu));
  
  %fprintf('k_2: %11.4f k_1: %11.4f m_2: %11.4f m_1: %11.4f\n', k_2, k_1, m_2, m_1);
  
  fprintf('BULK AND SHEAR (W/R TO HS BOUNDS), PERSENTS\n');
  
  % HASHIN-SHTRICKMAN BOUNDS
  k_U = k_2 + (1.-ro)/((1./(k_1 - k_2))+(ro/(k_2 + m_2)));
  k_L = k_1 + ro /((1./(k_2 - k_1))+((1. - ro)/(k_1 + m_1)));
  m_U = m_2 + (1.-ro) /((1./(m_1 - m_2))+((ro*(k_2 + 2.*m_2))/(2.*m_2*(k_2 + m_2))));
  m_L = m_1 + ro /((1./(m_2 - m_1))+(((1.-ro)*(k_1 + 2.*m_1))/(2.*m_1*(k_1 + m_1))));
  
  %fprintf('k_U: %11.4f k_L: %11.4f m_U: %11.4f m_L: %11.4f\n', k_U, k_L, m_U, m_L);
  
  bulk_pers = 100 * (bulk - k_L) / (k_U - k_L);
  shear_pers = 100 * (shear - m_L) / (m_U - m_L);
  
  fprintf('BULK: %11.4f persent,             SHEAR %11.4f persent \n', bulk_pers, shear_pers);
  
 imwrite(logical(ones(nely, nelx) - xPhys),'../initial guesses/20004.bmp')
  
  figure(1);
  %% PLOT DENSITIES
  colormap(gray); imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
  %% PRINT RESULTING BULK AND SHEAR IN FIGURE
  title(['BULK:   ' num2str(bulk) '   SHEAR:   ' num2str(shear)]);
  
  
  figure(2);
  plot(loop_log, iso_log, 'red'); % Plot isotropy parameter
  hold on;
  plot(loop_log, cost_log, 'blue'); % Plot functional
  hold off;
  

end