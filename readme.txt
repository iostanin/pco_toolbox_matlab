Periodic cell optimization toolbox for Matlab
based on topology optimization codes by 

I. Ostanin, 2017
L. Xia, 2015
N. Aage, 2011
O. Sigmund, 2003
K. Swanberg, 1999

Overview

The code evolved from the optimization routine by Xia (/Original_codes/topX_orig)

We extended it with different IO options, control of an optimized functional, 
control of isotropy, logging and various cost functionals and their sensitivities,
different optimization techniques.

Based on the homogenizer and FEM solver by L. Xia, we created a system of the analysis 
of the low-parametric structures (Main.m)