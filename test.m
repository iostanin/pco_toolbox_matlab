load('sweep_results_file_octa_nu_0.mat')

% G-closure with p_4 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_2,'filled');
axis([0 K_base 0 G_base])
grid on 
colorbar

print('sweep_results/bulk_shear_p_2', '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_2,'filled');
axis([-1 1 0 1])
grid on
colorbar

print('sweep_results/young_poiss_p_2', '-dpng');
