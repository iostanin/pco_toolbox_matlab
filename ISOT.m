function [ K ] = ISOT( E, nu )
% Generates isotropic stiffness matrix

K = [ E /(1 - nu^2), nu * E /(1 - nu^2),  0;...
      nu * E /(1 - nu^2), E /(1 - nu^2),  0;...
      0,                  0,              E / (2 * (1 + nu))];

end

