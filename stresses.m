function [ S ] = stresses(node, E, nu, v, d)
% This function computes stresses based on the maps of displacements
        if (node==1)
            csi = -1;
            eta = -1;
        end
        if (node==2)
            csi =  1;
            eta = -1;
        end
        if (node==3)
            csi =  1;
            eta =  1;
        end
        if (node==4)
            csi = -1;
            eta =  1;
        end

        % Element stiffness matrix
        D = ISOT(E, nu);
        
        N1csi = - 0.25 * (1 - eta);
        N1eta = - 0.25 * (1 - csi);
        
        N2csi = 0.25 * (1 - eta);
        N2eta = - 0.25 * (1 + csi);
        
        N3csi = 0.25 * (1 + eta);
        N3eta = 0.25 * (1 + csi);
        
        N4csi = -0.25 * (1 + eta);
        N4eta = 0.25 * (1 - csi);
        
        x1 = v(1);
        y1 = v(2);
        x2 = v(3);
        y2 = v(4);
        x3 = v(5);
        y3 = v(6);
        x4 = v(7);
        y4 = v(8);
        
        x_csi = x1 * N1csi + x2 * N2csi + x3 * N3csi + x4 * N4csi;
        x_eta = x1 * N1eta + x2 * N2eta + x3 * N3eta + x4 * N4eta;
        y_csi = y1 * N1csi + y2 * N2csi + y3 * N3csi + y4 * N4csi;
        y_eta = y1 * N1eta + y2 * N2eta + y3 * N3eta + y4 * N4eta;
        
        J = x_csi * y_eta - x_eta * y_csi;
        

% B (strain-displacement) matrix
        B = zeros(3,8);
        B(1,1) = (1/J) * ( N1csi * y_eta - N1eta * y_csi);
        B(2,2) = (1/J) * (-N1csi * x_eta + N1eta * x_csi);
        B(3,1) = (1/J) * (-N1csi * x_eta + N1eta * x_csi);
        B(3,2) = (1/J) * ( N1csi * y_eta - N1eta * y_csi);
        B(1,3) = (1/J) * ( N2csi * y_eta - N2eta * y_csi);
        B(2,4) = (1/J) * (-N2csi * x_eta + N2eta * x_csi);
        B(3,3) = (1/J) * (-N2csi * x_eta + N2eta * x_csi);
        B(3,4) = (1/J) * ( N2csi * y_eta - N2eta * y_csi);
        B(1,5) = (1/J) * ( N3csi * y_eta - N3eta * y_csi);
        B(2,6) = (1/J) * (-N3csi * x_eta + N3eta * x_csi);
        B(3,5) = (1/J) * (-N3csi * x_eta + N3eta * x_csi);
        B(3,6) = (1/J) * ( N3csi * y_eta - N3eta * y_csi);
        B(1,7) = (1/J) * ( N4csi * y_eta - N4eta * y_csi);
        B(2,8) = (1/J) * (-N4csi * x_eta + N4eta * x_csi);
        B(3,7) = (1/J) * (-N4csi * x_eta + N4eta * x_csi);
        B(3,8) = (1/J) * ( N4csi * y_eta - N4eta * y_csi);
        
        
 S = D * (B * d');       

end

