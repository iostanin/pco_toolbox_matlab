
% Plot black triangle w anti-aliasing (1 - black, 0 - white)
function x = plotTriangle(size_i, size_j, triangle)
  X = 256 * ones(size_i, size_j, 3);
  %triangle = [x1 y1 x2 y2 x3 y3];
  X = insertShape(X,'FilledPolygon',{triangle},...
  'Color', {'red'},'Opacity',1.0);
  x = 1 - (X(:,:,1) - 1)/255.;
  x = 1 - (X(:,:,1) - 1)/255.;
  
end