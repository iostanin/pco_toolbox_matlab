% Processing sweep file to add some additional data


% 1) Loading the sweep file
load('sweep_results_file_GSIW_nu_00_add.mat')


% 2) Computing Elastic moduli of the nearest elasticity tensor in terms of 
% stiffness and compliance
young_S = zeros(size(Q_11));

for i = 1:size(Q_11)
    C = [Q_11(i), Q_12(i), Q_13(i); Q_21(i), Q_22(i), Q_23(i); Q_31(i), Q_32(i), Q_33(i)];
    S = inv(C);
    
    % Compliance parameters
    young_S(i) = 10/(3*S(1,1)+4*S(1,2)+2*S(3,3)+3*S(2,2));
    poiss_S(i) = -(2*(3*S(1,2)+S(1,1)-S(3,3)+S(2,2)))/(3*S(1,1)+4*S(1,2)+2*S(3,3)+3*S(2,2));
    bulk_S(i) = young_S(i) / (2 * (1 - poiss_S(i)));
    shear_S(i) = young_S(i) / (2 * (1 + poiss_S(i)));
    
    % Stiffness parameters
    young_C(i) = (4*(C(1,1)^2+C(3,3)*C(1,1)+2*C(2,2)*C(1,1)+C(2,2)*C(3,3)+C(2,2)^2+2*C(1,2)*C(3,3)-4*C(1,2)^2))/(4*C(3,3)+9*C(1,1)+2*C(1,2)+9*C(2,2));
    poiss_C(i) = (18*C(1,2)+C(2,2)-4*C(3,3)+C(1,1))/(4*C(3,3)+9*C(1,1)+2*C(1,2)+9*C(2,2));
    bulk_C(i) = young_C(i) / (2 * (1 - poiss_C(i)));
    shear_C(i) = young_C(i) / (2 * (1 + poiss_C(i)));
    
    % C - isotropy 
    
    C_iso = [ -young_C(i)/(-1+poiss_C(i)^2), -young_C(i)*poiss_C(i)/(-1+poiss_C(i)^2), 0;   -young_C(i)*poiss_C(i)/(-1+poiss_C(i)^2), -young_C(i)/(-1+poiss_C(i)^2), 0;   0, 0, (1/2)*young_C(i)/(1+poiss_C(i))];
    
    Iso_C(i) = norm(C - C_iso)/norm(C); 
    
    
    % S - isotropy
    
    S_iso = [1/young_S(i), -poiss_S(i)/young_S(i), 0;  -poiss_S(i)/young_S(i), 1/young_S(i), 0;  0, 0, (2*(1+poiss_S(i)))/young_S(i)];
    Iso_S(i) = norm(S - S_iso)/norm(S);
end



nu_base = 0.0;

A1 = nu_base / 2;
A2 = (nu_base+1) / 2;
A0 = 0;


right_coverage = 0;
for i = 1:size(young)
    if ( abs( young(i) - 0.5) < 0.05 ) 
        if (poiss(i)>A1)
            if (poiss(i)>A0)
                A0 = poiss(i);
                right_coverage = (A0 - A1) / (A2 - A1);
            end
        end
    end
end

right_coverage


left_coverage = 0;

A1 = nu_base / 2;
A2 = (nu_base-1) / 2;
A0 = 0;


for i = 1:size(young)
    if ( abs( young(i) - 0.5) < 0.05 ) 
        if (poiss(i)<A1)
            if (poiss(i)<A0)
                A0 = poiss(i);
                left_coverage = (A0 - A1) / (A2 - A1);
            end
        end
    end
end

left_coverage


% 2) 
save('sweep_results_file_GSIW_nu_00_add_processed.mat')
