% Hexacell with Milton style (discrete) stripes

function x = plotHexacellMilton(side, p_1, p_2, p_3, p_4, p_5, p_6)

 size_i = double( 2. * round(sqrt(3)* side/2., 0) );
 size_j = double(3. * side);
 
 x = zeros(size_i, size_j);
 
 % a) Plot primitives
 primitives = zeros(1,6);
 
 % a-1) Center triangle
  
 xd = 0.5 * sqrt(3.) * (1 - p_1) * side;
 yd = side * ( 1.5 - 0.5 * p_1 ) - 1;
 
 xe = 0.5 * sqrt(3.) * (1 - p_1) * side;
 ye = side * ( 1.5 + 0.5 * p_1 ) + 1;
  
 xa = 0.5 * sqrt(3.) * side + 1.;
 ya = 1.5 * side;
 
 triangle = [ya xa yd xd ye xe];
 primitives = [primitives; triangle];
 
 % a-2) Milton-stype stripes
 
 % Clone primitives to fullfill hexacell
 cl_primitives = clone_hex( side, primitives );

 % Test - plot primitives
 si = size(cl_primitives);
 for i = 2:si(1)
     
    % adjust vertical scale
    factor = double( 2. * round(sqrt(3)* side/2., 0) + 0.5) / ( sqrt(3)* side ); 
    prim = [ cl_primitives(i,1) cl_primitives(i,2) * factor cl_primitives(i,3) cl_primitives(i,4) * factor cl_primitives(i,5) cl_primitives(i,6) * factor];        
    x = x + plotTriangle(size_i, size_j, prim); x = min(x, 1.);
 end
 

 
 colormap(gray); 
 imagesc(1.-x); 
 caxis([0 1]); 
 axis equal; 
 axis off; 
 drawnow;
  
end

