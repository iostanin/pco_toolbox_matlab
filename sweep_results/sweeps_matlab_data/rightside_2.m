 clear
 E = 1;
 nu = 0.0;
 K = E / ( 2 * (1 - nu));
 G = E / ( 2 * (1 + nu));

 
 % sweep - 1
 load('sweep_getHEXMD_16_03_30_right_10_stripes.mat')

 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 P_5 = p_5;
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 1 * ones(size(p_1)); % number of sweep
 
 
   figure(2);
  colormap jet;
  scatter(POISS,YOUNG,[],SWEEP,'d','LineWidth', 1.5);
  axis([0 1 0 E]);
  grid on;
  colorbar;
  hold on;
 
 
 load('sweep_getHEXMD_16_51_01_right_10_stripes2.mat')
 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 P_5 = p_5;
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 2 * ones(size(p_1)); % number of sweep
 
 scatter(POISS,YOUNG,[],SWEEP,'x','LineWidth', 1.5);
 
 load('sweep_getHEXMD_17_48_48_right_10_stripes3.mat')
 
 P_1 = p_1;
 P_2 = p_2;
 P_3 = p_3;
 P_5 = p_5;
 
 
 VOLFRAC = volfrac;
 ISO = iso;
 YOUNG = young;
 POISS = poiss;
 SHEAR = shear;
 BULK  = bulk;
 
 STRUCT  = zeros(size(p_1)); % zero - HEX, one - HEXD
 SWEEP = 3 * ones(size(p_1)); % number of sweep
 
 
 
  %figure(2);
  %colormap jet;
  scatter(POISS,YOUNG,[],SWEEP,'+','LineWidth', 1.5);
  %axis([0 1 0 E]);
  %grid on;
  %colorbar;

 
 