% Try to plot rectangular cell with  

function x = plotHexacell(side, p_1, p_2, p_3, p_4)

 size_i = double( 2. * round(sqrt(3)* side/2., 0) );
 size_j = double(3. * side);
 
 %center = [ 0.5 * (1 + size_i)  0.5 * (1 + size_j) ];
 center = [ 0.5 * sqrt(3.) * side  1.5 * side ];
 
 x = zeros(size_i, size_j);
 
 % a) Plot primitives
 primitives = zeros(1,6);
 
 % a-1) Center triangle
 
 %x1 =  0.5 * sqrt(3.) * side * (1 - p_1);
 %y1 =  1.5 * side - 0.5 * side * p_1;
 
 %x2 =  0.5 * sqrt(3.) * side * (1 - p_1);
 %y2 =  1.5 * side + 0.5 * side * p_1;
 
 %x3 =  0.5 * sqrt(3.) * side;
 %y3 =  1.5 * side;
 
 x1 = center(1) - 0.5 * sqrt(3.) * side * p_1;
 y1 = center(2) - 0.5 * side * p_1 - 1;
 
 x2 = center(1) - 0.5 * sqrt(3.) * side * p_1;
 y2 = center(2) + 0.5 * side * p_1 + 1;
  
 x3 = center(1) + 1.;
 y3 = center(2);
 
 triangle = [y1 x1 y2 x2 y3 x3];
 primitives = [primitives; triangle];
 
 
 % a-2) Sigmund-stype stripes
 
 xa = center(1) - 0.5 * sqrt(3.) * side * p_1 + 1;
 ya = center(2) - 0.5 * side * p_1;
 
 xb = center(1) - 0.5 * sqrt(3.) * side * p_1 + 1;
 yb = center(2) + 0.5 * side * p_1;
 
 xe = center(1) - 0.5 * sqrt(3.) * side * p_1;
 ye = center(2) - 0.5 * side * p_1 + side * p_1 * p_3;
 
 xc = 0;
 yc = center(2) - 0.5 * side * p_1 * p_3;
 
 xd = 0;
 yd = center(2) + 0.5 * side * p_1 * p_3;
 
 
 L = side * p_1 * p_3;
 dy = L / (p_2 - 1 + p_4);
 
 xlb = xa; ylb = ya;
 xub = xc; yub = yc;
 xle = xa; yle = ylb + dy*p_4;
 xue = xc; yue = yub + dy*p_4;
 
 
 
 for i = 1:p_2
     
    triangle = [ylb xlb yub xub yle xle];
    primitives = [primitives; triangle];
 
    triangle = [yub xub yle xle yue xue];
    primitives = [primitives; triangle];
    
    ylb = ylb + dy;
    yub = yub + dy;
    yle = yle + dy;
    yue = yue + dy;
    
    
    
 end
 
 % Clone primitives to fullfill hexacell
 cl_primitives = clone_hex( side, primitives );

 % Test - plot primitives
 si = size(cl_primitives);
 for i = 2:si(1)
     
    % adjust vertical scale
    factor = double( 2. * round(sqrt(3)* side/2., 0) + 0.5) / ( sqrt(3)* side ); 
    prim = [ cl_primitives(i,1) cl_primitives(i,2) * factor cl_primitives(i,3) cl_primitives(i,4) * factor cl_primitives(i,5) cl_primitives(i,6) * factor];        
    x = x + plotTriangle(size_i, size_j, prim); x = min(x, 1.);
 end
 

 
 colormap(gray); 
 imagesc(1.-x); 
 caxis([0 1]); 
 axis equal; 
 axis off; 
 drawnow;
  
end

