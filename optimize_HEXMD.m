function [ res ] = optimize_HEXMD( E, nu, p_1, p_2, p_3, p_4, p_5, p_6 )
% Wrapper for the external optimizer

def = 100;
A  = getHEXMD(1, 0.0, def, p_1, p_2, p_3, p_4, p_5, p_6, 0, 0, 0);
res = [(A(3) - E)^2 + (A(4)-nu)^2 A(3) A(4)];

end

