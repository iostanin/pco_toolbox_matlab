% Exploration of the parameters of a SIGMUND SQUARE STRUCTURE
% Modified Sigmund strucuture

function [BS] = getSIX(E_base, nu_base, nelx,nely, p_1, p_2, p_3)
%% MATERIAL PROPERTIES
E0 = E_base;
Emin = 1e-9;
nu = nu_base;
%% PREPARE FINITE ELEMENT ANALYSIS
A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);
nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
edofMat = repmat(edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);

%% PERIODIC BOUNDARY CONDITIONS
e0 = eye(3);
ufixed = zeros(8,3);
U = zeros(2*(nely+1)*(nelx+1),3);
alldofs = (1:2*(nely+1)*(nelx+1));
n1 = [nodenrs(end,[1,end]),nodenrs(1,[end,1])];
d1 = reshape([(2*n1-1);2*n1],1,8);
n3 = [nodenrs(2:end-1,1)',nodenrs(end,2:end-1)];
d3 = reshape([(2*n3-1);2*n3],1,2*(nelx+nely-2));
n4 = [nodenrs(2:end-1,end)',nodenrs(1,2:end-1)];
d4 = reshape([(2*n4-1);2*n4],1,2*(nelx+nely-2));
d2 = setdiff(alldofs,[d1,d3,d4]);
for j = 1:3
  ufixed(3:4,j) =[e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[nelx;0];
  ufixed(7:8,j) = [e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[0;nely];
  ufixed(5:6,j) = ufixed(3:4,j)+ufixed(7:8,j);
end
wfixed = [repmat(ufixed(3:4,:),nely-1,1); repmat(ufixed(7:8,:),nelx-1,1)];
%% INITIALIZE ITERATION
qe = cell(3,3);
Q = zeros(3,3);
dQ = cell(3,3);

%x = ones(200,200) - imread('2D library/10.bmp') ;
%x = ones(200,200) - imread('2D library/7.bmp') ;
%x = ones(200,200) - imread('initial guesses/5.bmp') ;

% Make Sigmund square structure with given parameters


% 1) Square
x = zeros(200,200);
height = int16( double(nelx) * p_1 / 2. );
for i = 1:height
    for j = 1:height
        x(i, j) = 1;
    end
end

% 2) Stripes

for i = 1:p_2 % number of stripes
    A1 = int16( double(nelx) * p_1 / 2.);
    A2 = nelx/2;
    B1 = int16(1 + (i-1) * (double(height)/p_2));
    B2 = int16(1 + (i-1) * (double(height)/p_2) + (double(height)/p_2) * p_3);
    % draw stripe
    for ii = A1:A2
        for jj = B1:B2
            x(ii, jj + int16( p_3 * (ii - A1) )) = 1;
        end
    end
end

% 1) Square
%x = zeros(200,200);
%height = int16( double(nelx) * p_1 / 2. );
for i = 90:100
    for j = 1:50
        x(i, j) = 1;
    end
end

% Symmetric reflections

% Sym in a quarter
      for i = 2:nelx/2
          for j = 1:i-1
              x(j,i) = x(i,j);
          end
      end
      
      
      for i = 1:nelx/2
          for j = 1:nely/2
              x(nelx + 1 - i, nely + 1 - j) = x(i,j);
              x(i, nely + 1 - j) = x(i,j);
              x(nelx + 1 - i, j) = x(i,j);
          end
      end


xPhys = x;
change = 1;

%% FE-ANALYSIS
sK = reshape(KE(:)*(Emin+xPhys(:)'.^5*(E0-Emin)),64*nelx*nely,1);
K = sparse(iK,jK,sK); K = (K+K')/2;
Kr = [K(d2,d2), K(d2,d3)+K(d2,d4); K(d3,d2)+K(d4,d2), K(d3,d3)+K(d4,d3)+K(d3,d4)+K(d4,d4)];
U(d1,:) = ufixed;
U([d2,d3],:) = Kr\(-[K(d2,d1); K(d3,d1)+K(d4,d1)]*ufixed-[K(d2,d4); K(d3,d4)+K(d4,d4)]*wfixed);
U(d4,:) = U(d3,:)+wfixed;

%% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
  for i = 1:3
    for j = 1:3
      U1 = U(:,i); U2 = U(:,j);
      qe{i,j} = reshape(sum((U1(edofMat)*KE).*U2(edofMat),2),nely,nelx)/(nelx*nely);
      Q(i,j) = sum(sum((Emin+xPhys.^5*(E0-Emin)).*qe{i,j}));
    end
  end
  
  %% TOPOLOGY PARAMETERS QUICKSHEET
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  fprintf('==============================================================');
  fprintf('           MICROSTRUCTURE PARAMETERS QUICKSHEET               ');
  fprintf('==============================================================');
  fprintf('\n');
  
  fprintf('HOMOGENIZED ELASTIC CONSTANTS\n');
  fprintf('\n');
  fprintf('Q11: %11.4f\n', Q(1,1));
  fprintf('Q12: %11.4f\n', Q(1,2));
  fprintf('Q13: %11.4f\n', Q(1,3));
  
  fprintf('Q21: %11.4f\n', Q(2,1));
  fprintf('Q22: %11.4f\n', Q(2,2));
  fprintf('Q23: %11.4f\n', Q(2,3));
  
  fprintf('Q31: %11.4f\n', Q(3,1));
  fprintf('Q32: %11.4f\n', Q(3,2));
  fprintf('Q33: %11.4f\n', Q(3,3));
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  fprintf('ISOTROPY (RELATIVE)\n');
  iso = abs( (Q(1,2) + 2 * Q(3,3) - Q(1,1))/Q(1,1));
  fprintf('iso: %11.4f\n', iso);
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');

  %% BULK AND SHEAR MODULI
  fprintf('BULK AND SHEAR (ABSOLUTE)\n');
  bulk = 0.25 * ( Q(1,1) + Q(1,2) + Q(2,1) + Q(2,2));
  shear = Q(3,3);
  fprintf('BULK: %11.4f SHEAR %11.4f  \n', bulk, shear);
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  
  
  %% POISSON
  fprintf('YOUNG AND POISSON (ABSOLUTE)\n');
  young = 4. / ( 1./bulk + 1./shear );
  poiss = ( bulk - shear ) / ( bulk + shear );
  fprintf('YOUNG: %11.4f POISS: %11.4f \n', young, poiss);
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  
  
  %% VOLUME FRACTION AND LOCAL HS BOUNDS
  fprintf('VOLUME FRACTION AND LOCAL HS BOUNDS\n');
  ro =  mean(xPhys(:));
  fprintf('VOLUME FRACTION: %11.4f\n', ro);
 
  k_2 = E0 / (2*(1-nu));
  k_1 = Emin / (2*(1-nu));
  
  m_2 = E0 / (2*(1+nu));
  m_1 = Emin / (2*(1+nu));
  
  fprintf('HS BOUNDS:\n');
  
  % HASHIN-SHTRICKMAN BOUNDS
  k_U = k_2 + (1.-ro)/((1./(k_1 - k_2))+(ro/(k_2 + m_2)));
  k_L = k_1 + ro /((1./(k_2 - k_1))+((1. - ro)/(k_1 + m_1)));
  m_U = m_2 + (1.-ro) /((1./(m_1 - m_2))+((ro*(k_2 + 2.*m_2))/(2.*m_2*(k_2 + m_2))));
  m_L = m_1 + ro /((1./(m_2 - m_1))+(((1.-ro)*(k_1 + 2.*m_1))/(2.*m_1*(k_1 + m_1))));
  
  fprintf('K_max: %11.4f K_min: %11.4f G_max: %11.4f G_min: %11.4f\n', k_U, k_L, m_U, m_L);
  
  fprintf('BULK AND SHEAR (W/R TO HS BOUNDS), PERSENTS\n');
  bulk_pers = 100 * (bulk - k_L) / (k_U - k_L);
  shear_pers = 100 * (shear - m_L) / (m_U - m_L);
  BS = [bulk shear young poiss Q(1,1) Q(1,2) Q(1,3) Q(2,1) Q(2,2) Q(2,3) Q(3,1) Q(3,2) Q(3,3) iso ro k_U k_L m_U m_L bulk_pers shear_pers p_1 p_2 p_3];
  fprintf('BULK: %11.4f persent,             SHEAR %11.4f persent \n', bulk_pers, shear_pers);
  
  imwrite(logical(ones(nelx, nely) - xPhys),'initial guesses/intermed.bmp')
  
  %% PLOT DENSITIES
  colormap(gray); imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
