% Milton hex Dual strucutre


function [BS] = getHEXMD(E_base, nu_base, nel, p_1, p_2, p_3, p_4, p_5, p_6, verbose, drawnow, save_struct)

%% Box sizes
nelx = 2. * round(sqrt(3)* nel/2.);
nely = 3. * nel;


%% MATERIAL PROPERTIES
E0 = E_base;
Emin = 1e-6;
nu = nu_base;
%% PREPARE FINITE ELEMENT ANALYSIS

A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);
nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
edofVec = reshape(2*nodenrs(1:end-1,1:end-1)+1,nelx*nely,1);
edofMat = repmat(edofVec,1,8)+repmat([0 1 2*nely+[2 3 0 1] -2 -1],nelx*nely,1);
iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);

%% PERIODIC BOUNDARY CONDITIONS
e0 = eye(3);
ufixed = zeros(8,3);
U = zeros(2*(nely+1)*(nelx+1),3);
alldofs = (1:2*(nely+1)*(nelx+1));
n1 = [nodenrs(end,[1,end]),nodenrs(1,[end,1])];
d1 = reshape([(2*n1-1);2*n1],1,8);
n3 = [nodenrs(2:end-1,1)',nodenrs(end,2:end-1)];
d3 = reshape([(2*n3-1);2*n3],1,2*(nelx+nely-2));
n4 = [nodenrs(2:end-1,end)',nodenrs(1,2:end-1)];
d4 = reshape([(2*n4-1);2*n4],1,2*(nelx+nely-2));
d2 = setdiff(alldofs,[d1,d3,d4]);
for j = 1:3
  ufixed(3:4,j) =[e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[nelx;0];
  ufixed(7:8,j) = [e0(1,j),e0(3,j)/2;e0(3,j)/2,e0(2,j)]*[0;nely];
  ufixed(5:6,j) = ufixed(3:4,j)+ufixed(7:8,j);
end
wfixed = [repmat(ufixed(3:4,:),nely-1,1); repmat(ufixed(7:8,:),nelx-1,1)];
%% INITIALIZE ITERATION
qe = cell(3,3);
Q = zeros(3,3);
dQ = cell(3,3);

% Make Hexastrucutre
x = plotHexacellDualMilton(nel, p_1, p_2, p_3, p_4, p_5, p_6)';
xPhys = x;

%% FE-ANALYSIS
sK = reshape(KE(:)*(Emin+xPhys(:)'.^5*(E0-Emin)),64*nelx*nely,1);
K = sparse(iK,jK,sK); K = (K+K')/2;
Kr = [K(d2,d2), K(d2,d3)+K(d2,d4); K(d3,d2)+K(d4,d2), K(d3,d3)+K(d4,d3)+K(d3,d4)+K(d4,d4)];
U(d1,:) = ufixed;
U([d2,d3],:) = Kr\(-[K(d2,d1); K(d3,d1)+K(d4,d1)]*ufixed-[K(d2,d4); K(d3,d4)+K(d4,d4)]*wfixed);
U(d4,:) = U(d3,:)+wfixed;


%% STRESS AND DISPLACEMENTS EXTRACTION AND VISUALIZATION
if (drawnow==1)
problem = 2;
u = U(:, problem); % BVP # 1 stretching along i, #2 - stretching along j, #3 - shear
uu_x = zeros(nelx+1, nely+1);
uu_y = zeros(nelx+1, nely+1);
for i = 1:nelx+1
    for j = 1:nely+1
        uu_y(i,j) = u( 2 * ((nely+1)*(i-1) + j)     );
        uu_x(i,j) = u( 2 * ((nely+1)*(i-1) + j) - 1 );        
    end
end

% Compute stresses
s_xx = zeros(nelx, nely);
s_yy = zeros(nelx, nely);
s_xy = zeros(nelx, nely);
s_miz = zeros(nelx, nely);

for i = 2:nelx+1
    for j = 2:nely+1
        
        d = [uu_x(i-1,j-1), uu_y(i-1,j-1), uu_x(i,j-1), uu_y(i,j-1), ...
             uu_x(i,j), uu_y(i,j), uu_x(i-1,j), uu_y(i-1,j)];
         
        v = [ i-1, j-1, i, j-1, i,j, i-1,j];
        s = 0.25 * ( stresses(1,  E0, nu, v, d) + ...
                     stresses(2,  E0, nu, v, d) + ...
                     stresses(3,  E0, nu, v, d) + ...
                     stresses(4,  E0, nu, v, d));
                 
        %s = stresses(1,  E0, nu, v, d);         
                 
        s_xx(i-1, j-1) = s(1);
        s_yy(i-1, j-1) = -s(2);
        s_xy(i-1, j-1) = s(3);
        s_miz(i-1, j-1) = sqrt( s_xx(i-1, j-1) * s_xx(i-1, j-1)+ ...
                                s_yy(i-1, j-1) * s_yy(i-1, j-1)+ ...
                                3 * s_xy(i-1, j-1) * s_xy(i-1, j-1) - ...
                                s_xx(i-1, j-1) * s_yy(i-1, j-1));
    
        
    end
end

% Mask empty regions
for i = 1:nelx
    for j = 1:nely
        if (x(j,i)==0)
            s_xx(i,j) = 0;
            s_yy(i,j) = 0;
            s_xy(i,j) = 0;
            s_miz(i,j) = 0;
            
            
            uu_x(i,j) = 0;
            uu_x(i+1,j) = 0;
            uu_x(i+1,j+1) = 0;
            uu_x(i,j+1) = 0;
                     
            uu_y(i,j) = 0;
            uu_y(i+1,j) = 0;
            uu_y(i+1,j+1) = 0;
            uu_y(i,j+1) = 0;
            
        end
    end
end


% Plot stresses and displacements
if (problem == 1) 
    comment = 'Horizontal tension: ';
end

if (problem == 2) 
    comment = 'Vertical tension: ';
end

if (problem == 3) 
    comment = 'Positive shear: ';
end

figure(1);
colormap(jet); imagesc(uu_x);  axis equal; axis off; colorbar; title([comment 'u_x']); drawnow;
figure(2);
colormap(jet); imagesc(uu_y);  axis equal; axis off; colorbar; title([comment 'u_y']); drawnow;
figure(3);
colormap(jet); imagesc(s_xx');  axis equal; axis off; colorbar;  title([comment '\sigma_{xx}']); drawnow;
figure(4);
colormap(jet); imagesc(s_yy');  axis equal; axis off; colorbar;  title([comment '\sigma_{yy}']);  drawnow;
figure(5);
colormap(jet); imagesc(s_xy');  axis equal; axis off; colorbar;  title([comment '\sigma_{xy}']);  drawnow;
figure(6);
colormap(jet); imagesc(s_miz');  axis equal; axis off; colorbar;  title([comment '\sigma_{miz}']);  drawnow;


end 




%% OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
  for i = 1:3
    for j = 1:3
      U1 = U(:,i); U2 = U(:,j);
      qe{i,j} = reshape(sum((U1(edofMat)*KE).*U2(edofMat),2),nely,nelx)/(nelx*nely);
      Q(i,j) = sum(sum((Emin+xPhys.^5*(E0-Emin)).*qe{i,j}));
    end
  end
  
  
  %% POST-PROCESSING
  % Simple isotropy measure
  iso = abs( (Q(1,2) + 2 * Q(3,3) - Q(1,1))/Q(1,1));
  
  % bulk and shear moduli (assumption of isotropy)
  bulk = 0.25 * ( Q(1,1) + Q(1,2) + Q(2,1) + Q(2,2));
  shear = Q(3,3);
  
  % young's modulus and poisson's ratio
  young = 4. / ( 1./bulk + 1./shear );
  poiss = ( bulk - shear ) / ( bulk + shear );
  
  % volume fraction and local hs bounds
  ro =  mean(xPhys(:));
  
  k_2 = E0 / (2*(1-nu));
  k_1 = Emin / (2*(1-nu));
  
  m_2 = E0 / (2*(1+nu));
  m_1 = Emin / (2*(1+nu));
  
  % Hashin-Shtrickmann bounds
  k_U = k_2 + (1.-ro)/((1./(k_1 - k_2))+(ro/(k_2 + m_2)));
  k_L = k_1 + ro /((1./(k_2 - k_1))+((1. - ro)/(k_1 + m_1)));
  m_U = m_2 + (1.-ro) /((1./(m_1 - m_2))+((ro*(k_2 + 2.*m_2))/(2.*m_2*(k_2 + m_2))));
  m_L = m_1 + ro /((1./(m_2 - m_1))+(((1.-ro)*(k_1 + 2.*m_1))/(2.*m_1*(k_1 + m_1))));
  
  % Bulk and shear as the percentage of HS bounds
  bulk_pers = 100 * (bulk - k_L) / (k_U - k_L);
  shear_pers = 100 * (shear - m_L) / (m_U - m_L);
  
  % Sum of young and poiss - for nu_base = 0 should be one for extremal
  % structs.
  summ = young + poiss;
  
  
  %% TOPOLOGY PARAMETERS QUICKSHEET
  if (verbose==1)
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  fprintf('==============================================================');
  fprintf('           MICROSTRUCTURE PARAMETERS QUICKSHEET               ');
  fprintf('==============================================================');
  fprintf('\n');
  
  fprintf('HOMOGENIZED ELASTIC CONSTANTS\n');
  fprintf('\n');
  fprintf('Q11: %11.4f\n', Q(1,1));
  fprintf('Q12: %11.4f\n', Q(1,2));
  fprintf('Q13: %11.4f\n', Q(1,3));
  
  fprintf('Q21: %11.4f\n', Q(2,1));
  fprintf('Q22: %11.4f\n', Q(2,2));
  fprintf('Q23: %11.4f\n', Q(2,3));
  
  fprintf('Q31: %11.4f\n', Q(3,1));
  fprintf('Q32: %11.4f\n', Q(3,2));
  fprintf('Q33: %11.4f\n', Q(3,3));
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  fprintf('ISOTROPY (RELATIVE)\n');
  
  fprintf('iso: %11.4f\n', iso);
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  
  fprintf('BULK AND SHEAR (ABSOLUTE)\n');
  fprintf('BULK: %11.4f SHEAR %11.4f  \n', bulk, shear);
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  
  fprintf('YOUNG AND POISSON (ABSOLUTE)\n');
  fprintf('YOUNG: %11.4f POISS: %11.4f \n', young, poiss);
  fprintf('\n');
  fprintf('\n');
  fprintf('\n');
  
  fprintf('VOLUME FRACTION AND LOCAL HS BOUNDS\n');
  fprintf('VOLUME FRACTION: %11.4f\n', ro); 
  fprintf('HS BOUNDS:\n');
  fprintf('K_max: %11.4f K_min: %11.4f G_max: %11.4f G_min: %11.4f\n', k_U, k_L, m_U, m_L);
  
  fprintf('BULK AND SHEAR (W/R TO HS BOUNDS), PERSENTS\n');
  fprintf('BULK: %11.4f persent,             SHEAR %11.4f persent \n', bulk_pers, shear_pers);
   
  fprintf('YOUNG: %11.4f POISS: %11.4f SUM: %11.4f \n', young, poiss, summ);

  end

 
  %% Put together all output parameters
  BS = [bulk shear young poiss Q(1,1) Q(1,2) Q(1,3) Q(2,1) Q(2,2) Q(2,3) Q(3,1) Q(3,2) Q(3,3) iso ro k_U k_L m_U m_L bulk_pers shear_pers p_1 p_2 p_3 p_4 p_5];
  
  
  %% Save the structure
  if (save_struct==1)
    imwrite( ones(nely, nelx) - xPhys, ['sweep_results/structures/HEX_p1_' num2str(p_1) 'p2_' num2str(p_2) 'p3_' num2str(p_3) 'p4_' num2str(p_4) 'p5_' num2str(p_5) '.bmp']);
  end
  
  %% Plot the structure
  if (drawnow==1)
    fprintf('Drawing picture:\n');
    figure(7);
    colormap(gray); imagesc(1-xPhys); caxis([0 1]); axis equal; axis off; drawnow;
  end
  
  