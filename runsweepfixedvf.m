function [ output_args_list ] = runsweepfixedvf( Struct_type, def, E, nu, w_min, w_max, w_step, vff, verbose, drawnow, save_struct )

% runsweep - performs the parametric study of the properties of the
% prescribed microstructure, with given base elastic properties

E = 1;
nu = 0.0;
K = E / ( 2 * (1 - nu));
G = E / ( 2 * (1 + nu));

l = 1;

%% Save sweep results (matlab data)
stype = char(Struct_type);

sweep_name = ['sweep_fixedvf_' stype '_' datestr(now,'HH_MM_SS')];

for ww = w_max:-w_step:w_min
    for vf = 0.99995:0.00001:0.99999    
        ss = get_S(vf, ww);
        fprintf(" Run  h. w parameters: %7.4f %7.4f %7.4f %7.4f %7.4f % 7.4f \n\n", ww, ss, 0, 0, 0, 0);
                    
                    A = Struct_type(E, nu, def, ss, ww, 0, 0, 0, 0, verbose, drawnow, save_struct);
                    
                    %% BASIC PARAMETERS
                    bulk(l) = A(1);
                    shear(l) = A(2);
                    young(l) = A(3);
                    poiss(l) = A(4);
                       
                    Q_11(l) = A(5);
                    Q_12(l) = A(6);
                    Q_13(l) = A(7);
                    Q_21(l) = A(8);
                    Q_22(l) = A(9);
                    Q_23(l) = A(10);
                    Q_31(l) = A(11);
                    Q_32(l) = A(12);
                    Q_33(l) = A(13);
            
                    iso(l) = A(14);
                    volfrac(l) = A(15);
            
                    K_max(l) = A(16);
                    K_min(l) = A(17);
                    G_max(l) = A(18);
                    G_min(l) = A(19);
           
                    bulk_pers(l) = A(20);
                    shear_pers(l) = A(21);
            
                    p_1(l) = A(22);
                    p_2(l) = A(23);
                    p_3(l) = A(24);
                    p_4(l) = A(25);
                    p_5(l) = A(26);
                    
                    
                    %% ADDITIONAL PARAMETERS        
                    
                    C = [Q_11(l), Q_12(l), Q_13(l); Q_21(l), Q_22(l), Q_23(l); Q_31(l), Q_32(l), Q_33(l)];
                    S = inv(C);
    
                    % Compliance parameters
                    young_S(l) = 10/(3*S(1,1)+4*S(1,2)+2*S(3,3)+3*S(2,2));
                    poiss_S(l) = -(2*(3*S(1,2)+S(1,1)-S(3,3)+S(2,2)))/(3*S(1,1)+4*S(1,2)+2*S(3,3)+3*S(2,2));
                    bulk_S(l) = young_S(l) / (2 * (1 - poiss_S(l)));
                    shear_S(l) = young_S(l) / (2 * (1 + poiss_S(l)));
    
                    % Stiffness parameters
                    young_C(l) = (4*(C(1,1)^2+C(3,3)*C(1,1)+2*C(2,2)*C(1,1)+C(2,2)*C(3,3)+C(2,2)^2+2*C(1,2)*C(3,3)-4*C(1,2)^2))/(4*C(3,3)+9*C(1,1)+2*C(1,2)+9*C(2,2));
                    poiss_C(l) = (18*C(1,2)+C(2,2)-4*C(3,3)+C(1,1))/(4*C(3,3)+9*C(1,1)+2*C(1,2)+9*C(2,2));
                    bulk_C(l) = young_C(l) / (2 * (1 - poiss_C(l)));
                    shear_C(l) = young_C(l) / (2 * (1 + poiss_C(l)));
    
                    % C - isotropy 
                    C_iso = [ -young_C(l)/(-1+poiss_C(l)^2), -young_C(l)*poiss_C(l)/(-1+poiss_C(l)^2), 0;   -young_C(l)*poiss_C(l)/(-1+poiss_C(l)^2), -young_C(l)/(-1+poiss_C(l)^2), 0;   0, 0, (1/2)*young_C(l)/(1+poiss_C(l))];
                    Iso_C(l) = norm(C - C_iso)/norm(C); 
    
                    % S - isotropy
                    S_iso = [1/young_S(l), -poiss_S(l)/young_S(l), 0;  -poiss_S(l)/young_S(l), 1/young_S(l), 0;  0, 0, (2*(1+poiss_S(l)))/young_S(l)];
                    Iso_S(l) = norm(S - S_iso)/norm(S);
                    
                    save(['sweep_results/sweeps_matlab_data/' sweep_name '.mat']);                    
                    l = l + 1;
    end
end





%% Save G-closure images
mkdir( pwd, [ 'sweep_results/plots/' sweep_name]);

figure(2);
colormap jet;
scatter(bulk,shear,[],volfrac,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_volfrac'], '-dpng');


figure(3);
colormap jet;
scatter(poiss,young,[],volfrac,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_volfrac'], '-dpng');
% G-closure with isotropy colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],iso,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_iso'], '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],iso,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_iso'], '-dpng');

% G-closure with p_1 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_1,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_p_1'], '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_1,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_p_1'], '-dpng');

% G-closure with p_2 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_2,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_p_2'], '-dpng');


figure(3);
colormap jet;
scatter(poiss,young,[],p_2,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_p_2'], '-dpng');

% G-closure with p_3 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_3,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_p_3'], '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_3,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_p_3'], '-dpng');

% G-closure with p_4 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_4,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_p_4'], '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_4,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_p_4'], '-dpng');

% G-closure with p_5 colormap
figure(2);
colormap jet;
scatter(bulk,shear,[],p_5,'filled');
axis([0 K 0 G])
grid on 
colorbar
print([ 'sweep_results/plots/' sweep_name '/bulk_shear_p_5'], '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,[],p_5,'filled');
axis([-1 1 0 E])
grid on
colorbar
print([ 'sweep_results/plots/' sweep_name '/young_poiss_p_5'], '-dpng');

% G-closure - GENERAL
figure(2);
scatter(bulk,shear,'red');
axis([0 K 0 G])
grid on 
print([ 'sweep_results/plots/' sweep_name '/bulk_shear'], '-dpng');

figure(3);
colormap jet;
scatter(poiss,young,'blue');
axis([-1 1 0 E])
grid on
print([ 'sweep_results/plots/' sweep_name '/young_poiss'], '-dpng');


% Save log 
fid = fopen('sweep_results/log.txt', 'a+');
fprintf(fid, ['sweep_results/sweeps_matlab_data/sweep_' stype '_' datestr(now,'HH_MM_SS') '.mat\n']);
fclose(fid);



end

